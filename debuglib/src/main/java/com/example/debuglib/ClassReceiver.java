package com.example.debuglib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class ClassReceiver extends BroadcastReceiver {

    private static final Object SEPERATOR = "\n";// use , as seperator
    String sb = "";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase(Constant.INTENTFILTER_TRACK_MY_ACTIVITIES)) {
            sb += intent.getStringExtra(Constant.ACTIVITY);
            sb += SEPERATOR;
            Log.d("CheckingActivity", sb);
        } else if (intent.getAction().equalsIgnoreCase(Constant.INTENTFILTER_TRACK_RETROFIT)) {
            Log.d("CheckingActivity", intent.getStringExtra(Constant.RETROFIT));
            sb = intent.getStringExtra(Constant.RETROFIT);
            sb += SEPERATOR;
        }
        writeFile(sb);
    }


    public void writeFile(String text) {
        File externalStorageDir = Environment.getExternalStoragePublicDirectory("logfile.txt");
        String dir = Environment.getExternalStorageDirectory()+File.separator+"myDirectory";
        //create folder
        File folder = new File(dir); //folder name
        folder.mkdirs();

        File myFile = new File(dir,"a.txt");
        Log.d("CheckingActivity", myFile.getPath() + "    null");
        if (myFile.exists()) {

        } else {
            try {
                myFile.createNewFile();
                Log.d("CheckingActivity",  "new");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            FileOutputStream  fOut = new FileOutputStream(myFile,true);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(text);
            myOutWriter.close();
            fOut.close();
            Log.d("CheckingActivity",  "exisiting");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
