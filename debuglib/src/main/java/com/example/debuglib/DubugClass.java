package com.example.debuglib;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public abstract class DubugClass extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        sendBoardCastInfo();
        setContentView(getLayoutXml());
        initDebug(savedInstanceState);
    }

    protected abstract void initDebug(Bundle savedInstanceState);

    protected abstract int getLayoutXml();

    public static void getApiInfo(){

    }

    public void sendBoardCastInfo(){
        Intent intent = new Intent();
        intent.setAction(Constant.INTENTFILTER_TRACK_MY_ACTIVITIES);
        intent.putExtra(Constant.ACTIVITY, getClass().getName());
        Log.d("CheckingActivityOn",getClass().getSimpleName());
        sendBroadcast(intent);
    }
//    @Override
//    protected void onDestroy() {
//        // TODO Auto-generated method stub
//        super.onDestroy();
//        Intent intent = new Intent();
//        intent.setAction(INTENTFILTER_REMOVE_MY_ACTIVITIES);
//        intent.putExtra("activityName", activity.getClass().getSimpleName());
//        sendBroadcast(intent);
//        setActivity(null);
//    }
}
