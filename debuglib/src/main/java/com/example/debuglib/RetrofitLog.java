package com.example.debuglib;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class RetrofitLog {


    public static void retrofitLog(Context context,String response){
        Log.d("checkingResponse",response);
        Intent intent = new Intent();
        intent.setAction(Constant.INTENTFILTER_TRACK_RETROFIT);
        intent.putExtra(Constant.RETROFIT,response );
        context.sendBroadcast(intent);
    }
}
