package com.example.debuglib;

public class Constant {
    public static final String INTENTFILTER_TRACK_MY_ACTIVITIES = "INTENTFILTER_TRACK_MY_ACTIVITIES";
    public static final String INTENTFILTER_TRACK_RETROFIT = "INTENTFILTER_TRACK_MY_RETROFIT";
    public static final String ACTIVITY = "activity";
    public static final String RETROFIT = "retrofit";
}
