package com.zeronebits.nepalstockwatch.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.data.Entry;
import com.zeronebits.nepalstockwatch.alerts.view.AlertsFragment;
import com.zeronebits.nepalstockwatch.companyprofile.view.CompanySearchActvity;
import com.zeronebits.nepalstockwatch.customViews.ScrimInsetsFrameLayout;
import com.zeronebits.nepalstockwatch.dashboard.view.DashBoardFragment;
import com.zeronebits.nepalstockwatch.floorsheet.view.FloorSheetFragment;
import com.zeronebits.nepalstockwatch.helper.SharedPreferencesHelper;
import com.zeronebits.nepalstockwatch.indices.view.Indices;
import com.zeronebits.nepalstockwatch.livetrading.view.LiveTradingFragment;
import com.zeronebits.nepalstockwatch.login.view.LoginActivity;
import com.zeronebits.nepalstockwatch.mainactivitynotlogin.view.MainActivityNotLogin;
import com.zeronebits.nepalstockwatch.portfolio.view.PortfolioFragment;
import com.zeronebits.nepalstockwatch.userupdate.view.UserUpdateFragment;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.UtilsDevice;
import com.zeronebits.nepalstockwatch.watchlist.view.WishlistActivity;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final static String sKEY_SAVED_POSITION = "savedPosition";
    private final static int sPOSITION_HOME = 0;
    private final static int sPOSITION_EXPLORE = 1;
    private static final int sDELAY_MILLIS = 250;

    public ArrayList<String> chartData;
    public ArrayList<Entry> chartValue;
    private Toolbar mToolbar;
    private Context mContext;
    private ImageView mHomeIcon;
    //    private ImageView mExploreIcon;
    private DrawerLayout mDrawerLayout;
    private int mCurrentPosition = sPOSITION_HOME;
    private ScrimInsetsFrameLayout mScrimInsetsFrameLayout;
    private FrameLayout mDashboard, mFloorsheet, mliveTrading, mProfile, mWatchList,mIndices,mAlerts,mAccount;
    ActionBarDrawerToggle actionBarDrawerToggle;

    @BindView(R.id.navigation_drawer_items_list_linearLayout_portfolio)
    FrameLayout mProtfolio;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ll_login_logout)
    CardView loginLogout;
    @BindView(R.id.tv_toolbar_title)
    public TextView toolbarTitle;
    @BindView(R.id.tv_login_status)
    TextView toolbarLoginStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init(savedInstanceState);
    }

    private void init(@NonNull final Bundle savedInstanceState) {
        bindResources();
        setToolbar();
        setToolBarTitle("Dashboard");
        setUpDrawer();
        restoreState(savedInstanceState);
    }

    private void bindResources() {
        mContext = this;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mDashboard = (FrameLayout) findViewById(R.id.navigation_drawer_items_list_linearLayout_dashboard);
        mFloorsheet = (FrameLayout) findViewById
                (R.id.navigation_drawer_items_list_linearLayout_floorsheet);
        mliveTrading = (FrameLayout) findViewById
                (R.id.navigation_drawer_items_list_linearLayout_live_trading);
        mProfile = (FrameLayout) findViewById
                (R.id.navigation_drawer_items_list_linearLayout_profile);
        mWatchList = (FrameLayout) findViewById(R.id.navigation_drawer_items_list_linearLayout_watchlist);
        mFloorsheet = (FrameLayout) findViewById(R.id.navigation_drawer_items_list_linearLayout_floorsheet);
        mIndices = (FrameLayout) findViewById(R.id.navigation_drawer_items_list_linearLayout_indices);
        mAlerts = (FrameLayout) findViewById(R.id.navigation_drawer_items_list_linearLayout_alerts);
        mAccount = (FrameLayout) findViewById(R.id.navigation_drawer_items_list_linearLayout_user_update);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_activity_DrawerLayout);
        mScrimInsetsFrameLayout = (ScrimInsetsFrameLayout)
                findViewById(R.id.main_activity_navigation_drawer_rootLayout);
    }

    void setToolbar() {
        setSupportActionBar(toolbar);
        loginLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferencesHelper sharedPreferencesHelper = SharedPreferencesHelper.getSharedInstance(MainActivity.this);
                sharedPreferencesHelper.clearLoginPreferences();
                Intent b = new Intent(MainActivity.this, MainActivityNotLogin.class);
                b.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                b.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                b.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(b);
                finish();
            }
        });
    }

    public void setToolBarTitle(String title) {
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarTitle.setText(title);

       /* toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.mipmap.back_arrow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });*/

        if(Constant.USERCODE.equalsIgnoreCase("")){
            toolbarLoginStatus.setText("Login");
        }else{
            toolbarLoginStatus.setText("Logout");
        }
    }


    private void setUpDrawer() {
        setUpDrawerAffordance();
        setUpDrawerMaxWidth();
        setUpDrawerClickListeners();
    }

    private void setUpDrawerAffordance() {
        actionBarDrawerToggle = new ActionBarDrawerToggle
                (
                        this,
                        mDrawerLayout,
                        mToolbar,
                        R.string.navigation_drawer_opened,
                        R.string.navigation_drawer_closed
                ) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                // Disables the burger/arrow animation by default
                super.onDrawerSlide(drawerView, 0);
            }


        };
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        actionBarDrawerToggle.setHomeAsUpIndicator(R.mipmap.nav_bar);
        mDrawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // event when click home button
                mDrawerLayout.openDrawer(Gravity.START, true);
            }
        });
    }

    private void setUpDrawerClickListeners() {
        mDashboard.setOnClickListener(this);
        mFloorsheet.setOnClickListener(this);
        mliveTrading.setOnClickListener(this);
        mProfile.setOnClickListener(this);
        mWatchList.setOnClickListener(this);
        mProtfolio.setOnClickListener(this);
        mIndices.setOnClickListener(this);
        mAlerts.setOnClickListener(this);
        mAccount.setOnClickListener(this);
    }

    private void setUpDrawerMaxWidth() {
        final int probableMinDrawerWidth = UtilsDevice.getScreenWidthInPx(this);
//        UtilsMiscellaneous.getThemeAttributeDimensionSize(this, android.R.attr.actionBarSize);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);


        final int maxDrawerWidth = metrics.widthPixels;
          /*  getResources()
        .getDimensionPixelSize(R.dimen.navigation_drawer_max_width);*/

        mScrimInsetsFrameLayout.getLayoutParams().width =
                Math.min(probableMinDrawerWidth, maxDrawerWidth);
    }

    @Override
    public void onClick(View view) {
        mDrawerLayout.closeDrawer(GravityCompat.START);

        if (!view.isSelected()) {
            if (view == mDashboard) {
                selectHomeFragment();
                setToolbarTitle("Dashboard");
            } else if (view == mFloorsheet) {
                getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame,
                        new FloorSheetFragment()).addToBackStack("back").commit();
                setToolbarTitle("Floorsheet");
            } else if (view == mProfile) {
                getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame,
                        new CompanySearchActvity()).addToBackStack("back").commit();
                setToolbarTitle("Profile");
            } else if (view == mWatchList) {
                getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame,
                        new WishlistActivity()).addToBackStack("back").commit();
                setToolbarTitle("WatchList");
            }else if (view == mliveTrading) {
                getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame,
                        new LiveTradingFragment()).addToBackStack("back").commit();
                setToolbarTitle("Live Trading");
            }else if(view == mProtfolio){
                getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame,
                        new PortfolioFragment()).addToBackStack("back").commit();
                setToolbarTitle("Portfolio");
            }else if(view == mIndices){
                getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame,
                        new Indices()).addToBackStack("back").commit();
                setToolbarTitle("Indices");
            }else if(view == mAlerts){
                getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame,
                        new AlertsFragment()).addToBackStack("back").commit();
                setToolbarTitle("Alerts");
            }else if(view == mAccount){
                getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame,
                        new UserUpdateFragment()).addToBackStack("back").commit();
                setToolbarTitle("Alerts");
            }
        }
    }

    private void restoreState(final @Nullable Bundle savedInstanceState) {
        // This allow us to know if the activity was recreated
        // after orientation change and restore the Toolbar title
        if (savedInstanceState != null) {
            switch (savedInstanceState.getInt(sKEY_SAVED_POSITION, sPOSITION_HOME)) {
                case sPOSITION_HOME:
                    selectHomeFragment();
                    break;

                default:
                    selectHomeFragment();
                    break;

            }
        } else {
            selectHomeFragment();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        setToolbarTitle("Dashboard");

    }

    private void selectHomeFragment() {
        mCurrentPosition = sPOSITION_HOME;
        deselectRows();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_content_frame, new DashBoardFragment())
                .commit();
    }


    private void setToolbarTitle(String string) {
        toolbarTitle.setText(string);
    }

    /**
     * We start this activities with delay to avoid junk while closing the drawer
     */
    private void startActivityWithDelay(@NonNull final Class activity) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(mContext, activity));
            }
        }, sDELAY_MILLIS);
    }

    private void deselectRows() {
        mDashboard.setSelected(false);
        mFloorsheet.setSelected(false);
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        outState.putInt(sKEY_SAVED_POSITION, mCurrentPosition);
        super.onSaveInstanceState(outState);
    }


  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        return true;
    }*/


}
