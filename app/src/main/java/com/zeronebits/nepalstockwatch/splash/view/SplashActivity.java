package com.zeronebits.nepalstockwatch.splash.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseActivity;
import com.zeronebits.nepalstockwatch.helper.SharedPreferencesHelper;
import com.zeronebits.nepalstockwatch.login.model.LoginUserModel;
import com.zeronebits.nepalstockwatch.login.presenter.LoginImp;
import com.zeronebits.nepalstockwatch.login.presenter.LoginPresenter;
import com.zeronebits.nepalstockwatch.login.view.LoginActivity;
import com.zeronebits.nepalstockwatch.login.view.LoginView;
import com.zeronebits.nepalstockwatch.mainactivitynotlogin.view.MainActivityNotLogin;
import com.zeronebits.nepalstockwatch.splash.presenter.SplashImp;
import com.zeronebits.nepalstockwatch.splash.presenter.SplashPresenter;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalwatchstock.R;

/**
 * Created by Own on 10/17/2017.
 */

public class SplashActivity extends BaseActivity implements SplashView{

    SharedPreferencesHelper sharedPreferencesHelper;
    SplashPresenter splashPresenter;

    @Override
    protected int getLayout() {
        return R.layout.splash_screen;
    }

    @Override
    protected void init() {
        sharedPreferencesHelper = SharedPreferencesHelper.getSharedInstance(this);
        Constant.ACESS_TOKEN = sharedPreferencesHelper.getLoginPreferecnes();
        Log.d("TOkenAcces",Constant.ACESS_TOKEN);
        splashPresenter = new SplashImp(SplashActivity.this,SplashActivity.this);
        splashPresenter.getCompanyIndexAndSubIndexDetail();

        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_SMS}, 100);
        }
    }

    @Override
    public void verifySucess() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void companyNameSucess() {
        if (Constant.ACESS_TOKEN.equalsIgnoreCase("")) {
            Intent i = new Intent(SplashActivity.this, MainActivityNotLogin.class);
            startActivity(i);
            finish();
        } else {
            SplashPresenter splashPresenter = new SplashImp(SplashActivity.this,SplashActivity.this);
            splashPresenter.verifyLogin(Constant.ACESS_TOKEN );
        }
    }

    @Override
    public void failedLogin() {
        Intent i = new Intent(SplashActivity.this, MainActivityNotLogin.class);
        startActivity(i);
        finish();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            Log.d("sdfdsfdsfdsf", "granted");
        }
    }
}
