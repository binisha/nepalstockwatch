package com.zeronebits.nepalstockwatch.watchlist.model;

import android.util.Log;

import com.zeronebits.nepalstockwatch.mainapplication.ApplicationMain;
import com.zeronebits.nepalstockwatch.realmdatabase.RealmController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Own on 10/23/2017.
 */

public class WatchListParser {

    String result;
    public ArrayList<WatchListUserModel> watchListUserModels;
    public WatchListParser(String result) {
        this.result = result;
        watchListUserModels = new ArrayList<>();
    }

    public void parser() throws JSONException {
        JSONArray jsonArray = new JSONArray(result);
        Log.d("dhfdsfdsf",result);
        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        realmController.clearAllWatchList();

        for(int i=0; i < jsonArray.length(); i++){
            JSONObject data = jsonArray.getJSONObject(i);
            WatchListUserModel watchListUserModel = new WatchListUserModel();
            WatchListModelDatabase watchListModelDatabase = new WatchListModelDatabase();
            watchListUserModel.setpRFLCODE(data.getString("PRFL_CODE"));
            watchListUserModel.seteFFFRMDT(data.getString("EFF_FRM_DT"));


            JSONObject jsonObject = data.getJSONObject("livetrading");
            watchListUserModel.setdATE(jsonObject.getString("DATE"));
            watchListUserModel.setcLOSE(jsonObject.getString("CLOSE"));
            watchListUserModel.setoPEN(jsonObject.getString("OPEN"));
            watchListUserModel.sethIGH(jsonObject.getString("HIGH"));
            watchListUserModel.setlOW(jsonObject.getString("LOW"));
            watchListUserModel.setvOLUME(jsonObject.getInt("VOLUME"));
            watchListUserModel.settURNOVER(jsonObject.getString("TURNOVER"));
            watchListUserModel.setdIFF(jsonObject.getString("DIFF"));
            watchListUserModel.setdIFFP(jsonObject.getString("DIFFP"));
            watchListUserModels.add(watchListUserModel);
            watchListModelDatabase.setCompanySymbol(data.getString("PRFL_CODE"));
            realmController.addWatchList(watchListModelDatabase);

        }
    }
}
