package com.zeronebits.nepalstockwatch.watchlist.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Own on 11/11/2017.
 */

public class WatchListUserModel  {

    @SerializedName("PRFL_CODE")
    @Expose
    private String pRFLCODE;
    @SerializedName("EFF_FRM_DT")
    @Expose
    private Object eFFFRMDT;
    @SerializedName("EFF_TO_DT")
    @Expose
    private Object eFFTODT;
    @SerializedName("CURR_FLG")
    @Expose
    private Object cURRFLG;
    @SerializedName("DATE")
    @Expose
    private String dATE;
    @SerializedName("CLOSE")
    @Expose
    private String cLOSE;
    @SerializedName("OPEN")
    @Expose
    private String oPEN;
    @SerializedName("HIGH")
    @Expose
    private String hIGH;
    @SerializedName("LOW")
    @Expose
    private String lOW;
    @SerializedName("VOLUME")
    @Expose
    private Integer vOLUME;
    @SerializedName("TURNOVER")
    @Expose
    private String tURNOVER;
    @SerializedName("DIFF")
    @Expose
    private String dIFF;
    @SerializedName("DIFFP")
    @Expose
    private String dIFFP;


    public String getpRFLCODE() {
        return pRFLCODE;
    }

    public void setpRFLCODE(String pRFLCODE) {
        this.pRFLCODE = pRFLCODE;
    }

    public Object geteFFFRMDT() {
        return eFFFRMDT;
    }

    public void seteFFFRMDT(Object eFFFRMDT) {
        this.eFFFRMDT = eFFFRMDT;
    }

    public Object geteFFTODT() {
        return eFFTODT;
    }

    public void seteFFTODT(Object eFFTODT) {
        this.eFFTODT = eFFTODT;
    }

    public Object getcURRFLG() {
        return cURRFLG;
    }

    public void setcURRFLG(Object cURRFLG) {
        this.cURRFLG = cURRFLG;
    }

    public String getdATE() {
        return dATE;
    }

    public void setdATE(String dATE) {
        this.dATE = dATE;
    }

    public String getcLOSE() {
        return cLOSE;
    }

    public void setcLOSE(String cLOSE) {
        this.cLOSE = cLOSE;
    }

    public String getoPEN() {
        return oPEN;
    }

    public void setoPEN(String oPEN) {
        this.oPEN = oPEN;
    }

    public String gethIGH() {
        return hIGH;
    }

    public void sethIGH(String hIGH) {
        this.hIGH = hIGH;
    }

    public String getlOW() {
        return lOW;
    }

    public void setlOW(String lOW) {
        this.lOW = lOW;
    }

    public Integer getvOLUME() {
        return vOLUME;
    }

    public void setvOLUME(Integer vOLUME) {
        this.vOLUME = vOLUME;
    }

    public String gettURNOVER() {
        return tURNOVER;
    }

    public void settURNOVER(String tURNOVER) {
        this.tURNOVER = tURNOVER;
    }

    public String getdIFF() {
        return dIFF;
    }

    public void setdIFF(String dIFF) {
        this.dIFF = dIFF;
    }

    public String getdIFFP() {
        return dIFFP;
    }

    public void setdIFFP(String dIFFP) {
        this.dIFFP = dIFFP;
    }
}
