package com.zeronebits.nepalstockwatch.watchlist.view;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyname.model.CompanyNameModel;
import com.zeronebits.nepalstockwatch.companyname.view.CompanyNameAdapter;
import com.zeronebits.nepalstockwatch.companyname.view.CompanyNameView;
import com.zeronebits.nepalstockwatch.login.model.LoginParser;
import com.zeronebits.nepalstockwatch.mainapplication.ApplicationMain;
import com.zeronebits.nepalstockwatch.realmdatabase.RealmController;
import com.zeronebits.nepalstockwatch.splash.model.SplashParser;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.watchlist.model.WatchListModel;
import com.zeronebits.nepalstockwatch.watchlist.model.WatchListModelDatabase;
import com.zeronebits.nepalstockwatch.watchlist.model.WatchListUserModel;
import com.zeronebits.nepalstockwatch.watchlist.presenter.WatchListImp;
import com.zeronebits.nepalstockwatch.watchlist.presenter.WatchListPresenter;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Own on 9/2/2017.
 */

public class WishlistActivity extends BaseFragment implements WatchListView {

    @BindView(R.id.btn_watch_list_add)
    Button addWatchList;
    @BindView(R.id.et_watch_list_name)
    AutoCompleteTextView watchListName;
    @BindView(R.id.tt_empty_record)
    TextView emptyWatchList;
    @BindView(R.id.ll_watch_list)
    LinearLayout watchListLinear;
    CompanyNameAdapter companyNameAdapter;
    WatchListPresenter watchListPresenter;
    MainActivity mainActivity;
    @BindView(R.id.rv_watch_list)
    RecyclerView recyclerView;
    ArrayList<WatchListModelDatabase> watchListUserModels;
    @BindView(R.id.avi)
    LinearLayout loading;
    @BindView(R.id.fab_watch_list)
    FloatingActionButton fabWatchList;
    @BindView(R.id.ll_watch_list_add)
    LinearLayout watchListAdd;
    @Override
    protected int getLayout() {
        return R.layout.wishlist;
    }
    @Override
    public void onResume() {
        super.onResume();
        try {
            mainActivity = (MainActivity) getActivity();
            mainActivity.setToolBarTitle("WatchList");
        }catch (Exception e){

        }
    }
    @Override
    protected void init(View view) {
        loading.setVisibility(View.VISIBLE);
        watchListPresenter = new WatchListImp(this,getActivity());
        watchListPresenter.getWatchList(Constant.USERCODE);
        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        ArrayList<CompanyNameModel> companyNameModels = realmController.getModelList();
        watchListName.setThreshold(1);
        watchListUserModels = realmController.getWatchList();
        Log.d("dfsdjnfsdf",watchListUserModels.size() + " ");
        companyNameAdapter = new CompanyNameAdapter(getActivity(), R.layout.simple_text_view, companyNameModels);
        watchListName.setAdapter(companyNameAdapter);
    }

    @OnClick(R.id.btn_watch_list_add)
    public void addWatchList(){
        String watchListNameString = watchListName.getText().toString();
        Log.d("sdjfndfsdf",watchListNameString);
        if(watchListNameString.length() == 0){
            watchListName.setError("Required");
        }else{
            Log.d("djfndsnfdsf",Constant.USERCODE);
            WatchListModel watchListModel = new WatchListModel(watchListNameString,Constant.USERCODE);
            watchListPresenter.addWatchList(watchListModel);
        }
    }

    @Override
    public void emptyWatchList() {
        loading.setVisibility(View.GONE);
        emptyWatchList.setVisibility(View.VISIBLE);
        watchListLinear.setVisibility(View.GONE);
    }

    @Override
    public void userWatchList(ArrayList<WatchListUserModel> watchListUserModels) {
        loading.setVisibility(View.GONE);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        WatchListAdapter watchListAdapter = new WatchListAdapter(watchListUserModels, getActivity(),this);
        recyclerView.setAdapter(watchListAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
                watchListAdd.setVisibility(View.GONE);
                fabWatchList.show();
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    @OnClick(R.id.fab_watch_list)
    public void showWatchListForm(){
        watchListAdd.setVisibility(View.VISIBLE);
        watchListName.requestFocus();
        fabWatchList.hide();
    }

    @Override
    public void deleteWatchList(String s) {
        watchListPresenter.deleteWatchList(s);
    }

    @Override
    public void deleteSucess() {
        watchListPresenter.getWatchList(Constant.USERCODE);
    }



}
