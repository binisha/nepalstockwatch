package com.zeronebits.nepalstockwatch.watchlist.presenter;

import com.zeronebits.nepalstockwatch.watchlist.model.WatchListModel;

/**
 * Created by Own on 10/23/2017.
 */

public interface WatchListPresenter {
    void addWatchList(WatchListModel watchListModel);

    void getWatchList(String usercode);

    void deleteWatchList(String s);
}
