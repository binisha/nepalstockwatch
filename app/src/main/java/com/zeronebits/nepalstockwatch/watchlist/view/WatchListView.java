package com.zeronebits.nepalstockwatch.watchlist.view;

import com.zeronebits.nepalstockwatch.watchlist.model.WatchListUserModel;

import java.util.ArrayList;

/**
 * Created by Own on 10/23/2017.
 */

public interface WatchListView {
    void emptyWatchList();

    void userWatchList(ArrayList<WatchListUserModel> watchListUserModels);

    void deleteWatchList(String s);

    void deleteSucess();
}
