package com.zeronebits.nepalstockwatch.livetrading.model;

import android.util.Log;

import com.zeronebits.nepalstockwatch.floorsheet.model.FloorSheetTableModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Own on 9/12/2017.
 */

public class LiveTradingParser {
    String result;
    ArrayList<String> headers;
    ArrayList<String> contents;
    ArrayList<String> diffPercentage;
    ArrayList<String> diffValue;

    public ArrayList<String> getDiffPercentage() {
        return diffPercentage;
    }

    public ArrayList<String> getDiffValue() {
        return diffValue;
    }

    ArrayList<ArrayList<String>> contentsWithValue;

    public ArrayList<String> getHeaders() {
        return headers;
    }

    public ArrayList<String> getContents() {
        return contents;
    }

    public ArrayList<ArrayList<String>> getContentsWithValue() {
        return contentsWithValue;
    }

    public LiveTradingParser(String result) {
        this.result = result;
        headers = new ArrayList<>();
        contentsWithValue = new ArrayList<>();
        diffPercentage = new ArrayList<>();
        diffValue = new ArrayList<>();
    }

    public void parse() throws JSONException {
        Log.d("jdfnsdfdsf",result);
        JSONObject jsonObject = new JSONObject(result);
        JSONArray data = jsonObject.getJSONArray("data");
        JSONArray headerJson = jsonObject.getJSONArray("header");
        for (int h = 0; h < headerJson.length(); h++) {
            headers.add(headerJson.getString(h));
        }

        for (int i = 0; i < data.length(); i++) {
            JSONObject jsonObject1 = data.getJSONObject(i);
            contents = new ArrayList<>();
            for (int a = 0; a < jsonObject1.length(); a++) {
                contents.add(jsonObject1.getString(jsonObject1.names().getString(a)));

                if (a == (jsonObject1.length() - 1)) {
                    diffValue.add(jsonObject1.getString(jsonObject1.names().getString(a)));
                }

                if (a == (jsonObject1.length() - 2)) {

                    diffPercentage.add(jsonObject1.getString(jsonObject1.names().getString(a)));
                }

            }

            contentsWithValue.add(contents);
        }
    }
}
