package com.zeronebits.nepalstockwatch.livetrading.presenter;

/**
 * Created by Own on 9/2/2017.
 */

public interface LiveTradingPresenter {

    void getLiveTradingData();
}
