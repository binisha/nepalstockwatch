package com.zeronebits.nepalstockwatch.mainapplication;

import android.app.Application;
import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Own on 11/5/2017.
 */

public class ApplicationMain extends Application {


    public static ApplicationMain instance;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        Realm.init(getApplicationContext());

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("nsw")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    public static Application getContext(){
        return instance;
    }

}
