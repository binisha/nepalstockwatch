package com.zeronebits.nepalstockwatch.alerts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Own on 12/3/2017.
 */

public class AlertsModel implements Serializable{


    @SerializedName("DESCR")
    @Expose
    private String dESCR;
    @SerializedName("CODE")
    @Expose
    private int cODE;
    @SerializedName("CATEGORY")
    @Expose
    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getdESCR() {
        return dESCR;
    }

    public void setdESCR(String dESCR) {
        this.dESCR = dESCR;
    }

    public int getcODE() {
        return cODE;
    }

    public void setcODE(int cODE) {
        this.cODE = cODE;
    }
}
