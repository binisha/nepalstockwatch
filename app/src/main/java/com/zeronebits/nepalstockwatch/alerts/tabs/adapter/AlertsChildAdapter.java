package com.zeronebits.nepalstockwatch.alerts.tabs.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.zeronebits.nepalstockwatch.alerts.model.AlertsModel;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlertsChildAdapter extends RecyclerView.Adapter<AlertsChildAdapter.MyViewHolder> {

    Context context;
    ArrayList<AlertsModel> alertsModels;
    boolean checked = false;

    public AlertsChildAdapter(Context context, ArrayList<AlertsModel> alertsModels) {
        this.context = context;
        this.alertsModels = alertsModels;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.alerts_child_single,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Log.d("CheckingSize", alertsModels.size()+"  ");
        AlertsModel alertsModel = alertsModels.get(position);
        holder.alertsName.setText(alertsModel.getdESCR());
        if(checked){
            holder.alertsName.setChecked(true);
        }else{
            holder.alertsName.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return alertsModels.size();
    }

    public void setAllCheck() {
        checked = true;
        notifyDataSetChanged();
    }

    public void setAllUnCheck() {
        checked = false;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.cb_alerts_name)
        CheckBox alertsName;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
