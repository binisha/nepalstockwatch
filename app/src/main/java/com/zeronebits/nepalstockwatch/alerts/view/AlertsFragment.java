package com.zeronebits.nepalstockwatch.alerts.view;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.alerts.tabs.AlertsMessage;
import com.zeronebits.nepalstockwatch.alerts.tabs.SelectAlerts;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.mainactivitynotlogin.view.MainActivityNotLogin;
import com.zeronebits.nepalstockwatch.portfolio.view.PorfolioTabAdapter;
import com.zeronebits.nepalwatchstock.R;

import butterknife.BindView;

/**
 * Created by Own on 12/3/2017.
 */

public class AlertsFragment extends BaseFragment {


    MainActivity mainActivity;
    MainActivityNotLogin mainActivityNotLogin;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.pager)
    ViewPager viewPager;

    @Override
    protected int getLayout() {
        return R.layout.alerts_fragment;
    }

    @Override
    protected void init(View view) {
        tabs.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(2);
        createViewPagerGuest(viewPager);
        createTabIconsGuest();
    }

    private void createViewPagerGuest(ViewPager viewPager) {
        PorfolioTabAdapter adapter = new PorfolioTabAdapter(getChildFragmentManager());
        adapter.addFrag(new SelectAlerts(), "Select Alerts");
        adapter.addFrag(new AlertsMessage(), "AlertsMessage");
        viewPager.setAdapter(adapter);

    }

    private void createTabIconsGuest() {

        TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabOne.setText("Select Alerts");
        tabs.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabTwo.setText("AlertsMessage");
        tabs.getTabAt(1).setCustomView(tabTwo);

    }


    @Override
    public void onResume() {
        super.onResume();

//        dashboardTable.setVisibility(View.GONE);
        try {
            mainActivity = (MainActivity) getActivity();
            mainActivity.setToolBarTitle("Alerts");
        } catch (Exception e) {
            mainActivityNotLogin = (MainActivityNotLogin) getActivity();
        }
    }
}
