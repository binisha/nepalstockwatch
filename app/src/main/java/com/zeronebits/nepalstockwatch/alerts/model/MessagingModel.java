package com.zeronebits.nepalstockwatch.alerts.model;

import io.realm.RealmObject;

/**
 * Created by Own on 1/7/2018.
 */

public class MessagingModel extends RealmObject {


    String title;
    String message;
    String id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
