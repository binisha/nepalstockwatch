package com.zeronebits.nepalstockwatch.alerts.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.alerts.model.AlertsModel;
import com.zeronebits.nepalstockwatch.alerts.model.UserAlertsModel;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlertsCategoryAdapter extends RecyclerView.Adapter<AlertsCategoryAdapter.ViewHolder> {

    HashMap<String,ArrayList<UserAlertsModel>> categoryWithUserAlertsModel;
    Context context;
    ArrayList<AlertsModel> alertsModels;
    AlertsView alertsView;
    ArrayList<String> alerts;

    public AlertsCategoryAdapter(HashMap<String,ArrayList<UserAlertsModel>> categoryWithUserAlertsModel, Context context,
                                 ArrayList<AlertsModel> alertsModels,ArrayList<String> alerts, AlertsView alertsView) {
        this.context = context;
        this.alerts = alerts;
        this.alertsView = alertsView;
        this.alertsModels = alertsModels;
        this.categoryWithUserAlertsModel = categoryWithUserAlertsModel;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_list_alerts_category_single, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String value = alerts.get(position);

        holder.categoryName.setText(value);
        UserAlertsListAdapter userAlertsListAdapter = new UserAlertsListAdapter(categoryWithUserAlertsModel.get(value)
                ,context,alertsModels,alertsView);
        holder.deleteItem.setAdapter(userAlertsListAdapter);


    }


    @Override
    public int getItemCount() {
        return categoryWithUserAlertsModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_alerts_category)
        TextView categoryName;
        @BindView(R.id.rv_user_alerts)
        RecyclerView deleteItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}