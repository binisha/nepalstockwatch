package com.zeronebits.nepalstockwatch.alerts.tabs;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;

import com.zeronebits.nepalstockwatch.alerts.model.AlertsModel;
import com.zeronebits.nepalstockwatch.alerts.tabs.adapter.AddAlertsCategoryAdapter;
import com.zeronebits.nepalstockwatch.alerts.view.AlertsCategoryAdapter;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyname.model.CompanyNameModel;
import com.zeronebits.nepalstockwatch.companyname.view.CompanyNameAdapter;
import com.zeronebits.nepalstockwatch.mainapplication.ApplicationMain;
import com.zeronebits.nepalstockwatch.realmdatabase.RealmController;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.watchlist.model.WatchListModelDatabase;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;


public class AddAlertsFragment extends BaseFragment {
    @BindView(R.id.rv_alerts_category)
    RecyclerView recyclerView;
    HashMap<String, ArrayList<AlertsModel>> arrayListHashMap;
    ArrayList<String> strings;
    @BindView(R.id.et_watch_list_name)
    AutoCompleteTextView watchListName;
    ArrayList<WatchListModelDatabase> watchListUserModels;
    CompanyNameAdapter companyNameAdapter;


    @Override
    protected int getLayout() {
        return R.layout.add_alerts;
    }

    @Override
    protected void init(View view) {
        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        ArrayList<CompanyNameModel> companyNameModels = realmController.getModelList();
        watchListName.setThreshold(1);
        watchListUserModels = realmController.getWatchList();
        Log.d("dfsdjnfsdf", watchListUserModels.size() + " ");
        companyNameAdapter = new CompanyNameAdapter(getActivity(), R.layout.simple_text_view, companyNameModels);
        watchListName.setAdapter(companyNameAdapter);

        strings = getArguments().getStringArrayList(Constant.ALERTSCATEGORY);
        arrayListHashMap = (HashMap<String, ArrayList<AlertsModel>>) getArguments().getSerializable(Constant.ALERTS);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        AddAlertsCategoryAdapter addAlertsCategoryAdapter = new AddAlertsCategoryAdapter(getActivity(), arrayListHashMap, strings);
        recyclerView.setAdapter(addAlertsCategoryAdapter);
    }
}
