package com.zeronebits.nepalstockwatch.alerts;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.alerts.model.AlertsModel;
import com.zeronebits.nepalstockwatch.alerts.model.MessagingModel;
import com.zeronebits.nepalstockwatch.alerts.model.UserAlertsModel;
import com.zeronebits.nepalstockwatch.alerts.view.AlertsView;
import com.zeronebits.nepalstockwatch.alerts.view.UserAlertsListAdapter;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Own on 1/7/2018.
 */

public class AlertsMessageAdapter extends RecyclerView.Adapter<AlertsMessageAdapter.ViewHolder> {

    ArrayList<MessagingModel> messagingModels;
    Context context;

    public AlertsMessageAdapter(ArrayList<MessagingModel> messagingModels, Context context) {
        this.messagingModels = messagingModels;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.alerts_messaging_single,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final MessagingModel messagingModel = messagingModels.get(position);
        holder.alertsName.setText(messagingModel.getMessage());
    }


    @Override
    public int getItemCount() {
        return messagingModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_message)
        TextView alertsName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}