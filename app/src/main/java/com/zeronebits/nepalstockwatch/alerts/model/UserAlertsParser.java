package com.zeronebits.nepalstockwatch.alerts.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Own on 12/7/2017.
 */

public class UserAlertsParser {
    String result;
    public ArrayList<UserAlertsModel> userAlertsModels;
    public HashMap<String,ArrayList<UserAlertsModel>> categoryWithUserAlertsModel;
    public ArrayList<String> alerts;
    public UserAlertsParser(String result) {
        this.result = result;
        userAlertsModels = new ArrayList<>();
        categoryWithUserAlertsModel = new HashMap<>();
        alerts = new ArrayList<>();
    }

    public void parser() throws JSONException {
        Log.d("checkingdata",result);
        JSONObject jsonObject = new JSONObject(result);
        for(int a=0; a < jsonObject.length(); a++){
            JSONArray json = jsonObject.getJSONArray(jsonObject.names().getString(a));
            alerts.add(jsonObject.names().getString(a));
            Log.d("CjeckingDatra",jsonObject.names().getString(a));
            for(int i =0; i < json.length(); i++){
                UserAlertsModel userAlertsModel = new UserAlertsModel();
                JSONObject obj = json.getJSONObject(i);
                userAlertsModel.setCode(obj.getString("ALRT_CODE"));
                userAlertsModel.setDesc(obj.getString("PRFL_CODE"));
                userAlertsModels.add(userAlertsModel);
            }

            categoryWithUserAlertsModel.put(jsonObject.names().getString(a),userAlertsModels);
        }


    }
}
