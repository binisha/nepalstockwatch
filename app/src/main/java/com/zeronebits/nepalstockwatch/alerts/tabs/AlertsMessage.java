package com.zeronebits.nepalstockwatch.alerts.tabs;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.zeronebits.nepalstockwatch.alerts.AlertsMessageAdapter;
import com.zeronebits.nepalstockwatch.alerts.model.AlertsModel;
import com.zeronebits.nepalstockwatch.alerts.model.MessagingModel;
import com.zeronebits.nepalstockwatch.alerts.model.UserAlertsModel;
import com.zeronebits.nepalstockwatch.alerts.presenter.AlertsImp;
import com.zeronebits.nepalstockwatch.alerts.presenter.AlertsPresenter;
import com.zeronebits.nepalstockwatch.alerts.view.AlertsView;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.mainapplication.ApplicationMain;
import com.zeronebits.nepalstockwatch.realmdatabase.RealmController;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Own on 12/3/2017.
 */

public class AlertsMessage extends BaseFragment{

    AlertsPresenter alertsPresenter;
    ArrayList<MessagingModel> messagingModels;
    @BindView(R.id.rv_user_alerts)
    RecyclerView recyclerView;
    @BindView(R.id.ll_no_message)
    LinearLayout noMessage;
    @Override
    protected int getLayout() {
        return R.layout.alerts_message;
    }

    @Override
    protected void init(View view) {
        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        messagingModels = realmController.getMessageList();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        if(messagingModels.size() > 0){
            AlertsMessageAdapter alertsMessageAdapter = new AlertsMessageAdapter(messagingModels,getActivity());
            recyclerView.setAdapter(alertsMessageAdapter);
            noMessage.setVisibility(View.GONE);
        }else{
            noMessage.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }


}
