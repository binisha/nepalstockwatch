package com.zeronebits.nepalstockwatch.alerts.tabs.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.zeronebits.nepalstockwatch.alerts.model.AlertsModel;
import com.zeronebits.nepalstockwatch.alerts.model.AlertsParser;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddAlertsCategoryAdapter extends RecyclerView.Adapter<AddAlertsCategoryAdapter.MyViewHolder> {

    Context context;
    HashMap<String,ArrayList<AlertsModel>> arrayListHashMap;
    ArrayList<String> strings;

    public AddAlertsCategoryAdapter(Context context, HashMap<String, ArrayList<AlertsModel>> arrayListHashMap, ArrayList<String> strings) {
        this.context = context;
        this.arrayListHashMap = arrayListHashMap;
        this.strings = strings;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.alerts_category_single,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.categoryName.setText(strings.get(position));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        holder.childList.setLayoutManager(linearLayoutManager);
        holder.childList.setItemAnimator(new DefaultItemAnimator());

        final AlertsChildAdapter adapter = new AlertsChildAdapter(context,
                AlertsParser.getAlertsOfCategory(strings.get(position)));
        holder.childList.setAdapter(adapter);

        holder.categoryName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    adapter.setAllCheck();
                }else{
                    adapter.setAllUnCheck();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.rv_category_child)
        RecyclerView childList;
        @BindView(R.id.cb_category_name)
        CheckBox categoryName;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
