package com.zeronebits.nepalstockwatch.alerts.model;

/**
 * Created by Own on 1/22/2018.
 */

public class CustomModelAlerts {

    String name;
    boolean categoryOrNot;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCategoryOrNot() {
        return categoryOrNot;
    }

    public void setCategoryOrNot(boolean categoryOrNot) {
        this.categoryOrNot = categoryOrNot;
    }
}
