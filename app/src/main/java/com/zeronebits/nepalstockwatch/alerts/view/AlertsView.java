package com.zeronebits.nepalstockwatch.alerts.view;

import com.zeronebits.nepalstockwatch.alerts.model.AlertsModel;
import com.zeronebits.nepalstockwatch.alerts.model.CustomModelAlerts;
import com.zeronebits.nepalstockwatch.alerts.model.UserAlertsModel;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Own on 12/3/2017.
 */

public interface AlertsView {

    void sucessfull();

    void userAlertsList(HashMap<String,ArrayList<UserAlertsModel>> categoryWithUserAlertsModel,ArrayList<String> alerts);

    void deleteItem(String code, String desc);

    void alertsLists(HashMap<String, ArrayList<AlertsModel>> arrayListHashMap,ArrayList<String> strings);
}
