package com.zeronebits.nepalstockwatch.alerts.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Own on 12/4/2017.
 */

public class AlertsParser {

    String result;
    public static ArrayList<AlertsModel> alertsModels;
    public ArrayList<String> strings;
    public ArrayList<CustomModelAlerts> customModelAlertses;
    public HashMap<String, ArrayList<AlertsModel>> arrayListHashMap;
    String currentCategory = "";

    public AlertsParser(String result) {
        this.result = result;
        alertsModels = new ArrayList<>();
        customModelAlertses = new ArrayList<>();
        arrayListHashMap = new HashMap<>();
        strings = new ArrayList<>();
    }


    public void parser() throws JSONException {
        Log.d("fdskkfjsdf", result);
        JSONArray json = new JSONArray(result);
        for (int i = 0; i < json.length(); i++) {
            JSONObject data = json.getJSONObject(i);
            AlertsModel alertsModel = new AlertsModel();
            alertsModel.setcODE(data.getInt("CODE"));
            alertsModel.setdESCR(data.getString("DESCR"));
            alertsModel.setCategory(data.getString("CATEGORY"));
            alertsModels.add(alertsModel);

            if (currentCategory.equalsIgnoreCase(data.getString("CATEGORY"))) {
            } else {
                currentCategory = data.getString("CATEGORY");
                strings.add(currentCategory);
            }
        }

    }


    public static ArrayList<AlertsModel> getAlertsOfCategory(String category){
        ArrayList<AlertsModel> alerts = new ArrayList<>();
        for(AlertsModel alertsModel  : alertsModels){
            if(alertsModel.getCategory().equalsIgnoreCase(category)){
                alerts.add(alertsModel);
            }
        }
        return alerts;
    }
}
