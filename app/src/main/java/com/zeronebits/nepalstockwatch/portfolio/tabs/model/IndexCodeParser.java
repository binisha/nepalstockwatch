package com.zeronebits.nepalstockwatch.portfolio.tabs.model;

import android.util.Log;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Own on 11/9/2017.
 */

public class IndexCodeParser  {
    String result;
    public ArrayList<IndexCodeModel> indexCodeModels;
    public ArrayList<String> chartData;
    public ArrayList<PieEntry> chartValue;
    public IndexCodeParser(String result) {
        this.result = result;
        chartValue = new ArrayList<>();
        chartData = new ArrayList<>();
        indexCodeModels = new ArrayList<>();
    }

    public void parser() throws JSONException {
        Log.d("sdjfdsfdf",result);
        JSONArray jsonArray = new JSONArray(result);
        for(int i =0; i < jsonArray.length(); i++){
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            IndexCodeModel indexCodeModel = new IndexCodeModel();
            indexCodeModel.setIndexCode(jsonObject.getString("INDEX_CODE"));
            indexCodeModel.setUnits(jsonObject.getInt("units"));
            indexCodeModel.setValues(jsonObject.getInt("value"));
            indexCodeModels.add(indexCodeModel);
            chartData.add(jsonObject.getString("INDEX_CODE"));
            chartValue.add(new PieEntry(jsonObject.getInt("units"),jsonObject.getString("INDEX_CODE")));
        }
    }
}
