package com.zeronebits.nepalstockwatch.portfolio.tabs.model;

import android.util.Log;

import com.github.mikephil.charting.data.PieEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Own on 11/14/2017.
 */

public class IssueTypeParserPie {
    String result;
    public ArrayList<IssueTypeModelPie> issueTypeModelPies;
    public ArrayList<String> chartData;
    public ArrayList<PieEntry> chartValue;

    public IssueTypeParserPie(String result) {
        this.result = result;
        issueTypeModelPies = new ArrayList<>();
        chartValue = new ArrayList<>();
        chartData = new ArrayList<>();
        issueTypeModelPies = new ArrayList<>();
    }

    public void parser() throws JSONException {
        Log.d("sdjfdsfdf",result);
        JSONArray jsonArray = new JSONArray(result);
        for(int i =0; i < jsonArray.length(); i++){
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            IssueTypeModelPie issueTypeParserPie = new IssueTypeModelPie();
            issueTypeParserPie.setIssueType(jsonObject.getString("ISSU_TYPE_CODE"));
            issueTypeParserPie.setUnits(jsonObject.getString("units"));
            issueTypeParserPie.setValue(jsonObject.getString("value"));
            issueTypeModelPies.add(issueTypeParserPie);
            chartData.add(jsonObject.getString("ISSU_TYPE_CODE"));
            chartValue.add(new PieEntry(Integer.parseInt(jsonObject.getString("units").replace(",","")),jsonObject.getString("ISSU_TYPE_CODE")));
        }
    }
}
