package com.zeronebits.nepalstockwatch.portfolio.tabs.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Own on 11/14/2017.
 */

public class CompanyPortfolioParser {

    String result;

    public ArrayList<String> headers;
    public ArrayList<String> contents;
    public ArrayList<ArrayList<String>> contentsWithValue;

    public CompanyPortfolioParser(String result) {
        this.result = result;
        headers = new ArrayList<>();
        contentsWithValue = new ArrayList<>();
    }

    public void parser() throws JSONException {
        Log.d("hgdfdsflds",result);
        JSONObject jsonObject = new JSONObject(result);
        JSONArray data = jsonObject.getJSONArray("data");
        JSONArray headerJson = jsonObject.getJSONArray("header");
        for (int h = 0; h < headerJson.length(); h++) {
            headers.add(headerJson.getString(h));
        }

        for (int i = 0; i < data.length(); i++) {
            JSONObject jsonObject1 = data.getJSONObject(i);
            contents = new ArrayList<>();
            for (int a = 0; a < jsonObject1.length(); a++) {
                contents.add(jsonObject1.getString(jsonObject1.names().getString(a)));
            }

            contentsWithValue.add(contents);
        }
        }


}
