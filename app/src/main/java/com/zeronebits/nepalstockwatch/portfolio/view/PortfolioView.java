package com.zeronebits.nepalstockwatch.portfolio.view;

import com.zeronebits.nepalstockwatch.portfolio.model.IssueTypeModel;

import java.util.ArrayList;

/**
 * Created by Own on 10/25/2017.
 */

public interface PortfolioView {
    void issueTypeList(ArrayList<IssueTypeModel> issueTypeModels);

    void boughtAndSoldtableData(ArrayList<String> boughtHeadersArrayList, ArrayList<ArrayList<String>> boughtTableContentArrayStrings,
                                ArrayList<String> soldHeadersArrayList, ArrayList<ArrayList<String>> soldTableContentArrayStrings);

    void addedSucessfully();

    void errorAdding();
}
