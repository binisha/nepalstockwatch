package com.zeronebits.nepalstockwatch.portfolio.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Own on 11/7/2017.
 */

public class IssueTypeModel {

    @SerializedName("CODE")
    @Expose
    private String cODE;
    @SerializedName("NAME")
    @Expose
    private String nAME;
    @SerializedName("DESCR")
    @Expose
    private String dESCR;

    public String getCODE() {
        return cODE;
    }

    public void setCODE(String cODE) {
        this.cODE = cODE;
    }

    public String getNAME() {
        return nAME;
    }

    public void setNAME(String nAME) {
        this.nAME = nAME;
    }

    public String getDESCR() {
        return dESCR;
    }

    public void setDESCR(String dESCR) {
        this.dESCR = dESCR;
    }

}

