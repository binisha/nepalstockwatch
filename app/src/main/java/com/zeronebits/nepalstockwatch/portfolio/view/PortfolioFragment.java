package com.zeronebits.nepalstockwatch.portfolio.view;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.inqbarna.tablefixheaders.TableFixHeaders;
import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyname.model.CompanyNameModel;
import com.zeronebits.nepalstockwatch.companyname.view.CompanyNameAdapter;
import com.zeronebits.nepalstockwatch.companyprofile.view.CompanyProfile;
import com.zeronebits.nepalstockwatch.mainapplication.ApplicationMain;
import com.zeronebits.nepalstockwatch.portfolio.model.BuyStockModel;
import com.zeronebits.nepalstockwatch.portfolio.model.IssueTypeModel;
import com.zeronebits.nepalstockwatch.portfolio.model.SellStockModel;
import com.zeronebits.nepalstockwatch.portfolio.presenter.ProtfolioImp;
import com.zeronebits.nepalstockwatch.portfolio.presenter.ProtfolioPresenter;
import com.zeronebits.nepalstockwatch.portfolio.tabs.PortfolioByCompany;
import com.zeronebits.nepalstockwatch.portfolio.tabs.PortfolioByIndex;
import com.zeronebits.nepalstockwatch.portfolio.tabs.PortfolioByIssues;
import com.zeronebits.nepalstockwatch.portfolio.tabs.ProtfolioBuyAndSell;
import com.zeronebits.nepalstockwatch.realmdatabase.RealmController;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Own on 9/12/2017.
 */

public class PortfolioFragment extends BaseFragment implements PortfolioView {


    MainActivity mainActivity;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.pager)
    ViewPager viewPager;
    ProtfolioPresenter protfolioPresenter;

    @Override
    protected int getLayout() {
        return R.layout.portfolio_new;
    }

    @Override
    protected void init(View view) {
        tabs.setupWithViewPager(viewPager);
        createViewPager(viewPager);
        createTabIcons();
        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        protfolioPresenter = new ProtfolioImp(this, getActivity());

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mainActivity = (MainActivity) getActivity();
            mainActivity.setToolBarTitle("Portfolio");
        } catch (Exception e) {

        }
    }


    @Override
    public void issueTypeList(ArrayList<IssueTypeModel> issueTypeModels) {

    }

    @Override
    public void boughtAndSoldtableData(ArrayList<String> boughtHeadersArrayList, ArrayList<ArrayList<String>> boughtTableContentArrayStrings, ArrayList<String> soldHeadersArrayList, ArrayList<ArrayList<String>> soldTableContentArrayStrings) {

    }

    @Override
    public void addedSucessfully() {

    }

    @Override
    public void errorAdding() {
        Toast.makeText(mainActivity, "could not add", Toast.LENGTH_SHORT).show();
    }


    private void createViewPager(ViewPager viewPager) {
        PorfolioTabAdapter adapter = new PorfolioTabAdapter(getChildFragmentManager());
        adapter.addFrag(new PortfolioByCompany(), "Company");
        adapter.addFrag(new PortfolioByIndex(), "Index");
        adapter.addFrag(new PortfolioByIssues(), "Issue Type");
        adapter.addFrag(new ProtfolioBuyAndSell(), "Buy/Sell");
        viewPager.setAdapter(adapter);

    }

    private void createTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabOne.setText("Company");
        tabs.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabTwo.setText("Index");
        tabs.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabThree.setText("Issue Type");
        tabs.getTabAt(2).setCustomView(tabThree);

        TextView tabFour = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabFour.setText("Buy/Sell");
        tabs.getTabAt(3).setCustomView(tabFour);
    }


    public void openFragment(String s) {
        Fragment fragment = new CompanyProfile();
        Bundle b = new Bundle();
        b.putString(Constant.COMPANYCODE, s);
        fragment.setArguments(b);
        getFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame, fragment).addToBackStack("tag").commit();
    }
}
