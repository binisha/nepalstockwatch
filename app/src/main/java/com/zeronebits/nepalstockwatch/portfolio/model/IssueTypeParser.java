package com.zeronebits.nepalstockwatch.portfolio.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Own on 11/7/2017.
 */

public class IssueTypeParser {

    String result;
    public ArrayList<IssueTypeModel> issueTypeModels;

    public IssueTypeParser(String result) {
        this.result = result;
        issueTypeModels = new ArrayList<>();
    }

    public void parser() throws JSONException {
        JSONArray jsonArray = new JSONArray(result);
        for(int i =0; i < jsonArray.length(); i++){
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            IssueTypeModel issueTypeModel = new IssueTypeModel();
            issueTypeModel.setCODE(jsonObject.getString("CODE"));
            issueTypeModel.setDESCR(jsonObject.getString("DESCR"));
            issueTypeModel.setNAME(jsonObject.getString("NAME"));
            issueTypeModels.add(issueTypeModel);
        }
    }
}
