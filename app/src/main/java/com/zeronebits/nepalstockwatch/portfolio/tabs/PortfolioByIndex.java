package com.zeronebits.nepalstockwatch.portfolio.tabs;

import android.graphics.Color;
import android.icu.text.NumberFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.dashboard.model.TotalGainParser;
import com.zeronebits.nepalstockwatch.portfolio.tabs.model.IndexCodeModel;
import com.zeronebits.nepalstockwatch.portfolio.tabs.model.IssueTypeModelPie;
import com.zeronebits.nepalstockwatch.portfolio.tabs.presenter.PieChartImp;
import com.zeronebits.nepalstockwatch.portfolio.tabs.presenter.PieChartPresenter;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;

/**
 * Created by Own on 11/9/2017.
 */

public class PortfolioByIndex extends BaseFragment implements PieChartView {

    @BindView(R.id.piechart)
    PieChart pieChart;

    @BindView(R.id.tv_total_gain)
    TextView totalGain;
    @BindView(R.id.avi)
    LinearLayout loading;
    String currencyText;

    @Override
    protected int getLayout() {
        return R.layout.portfolio_pie_chart;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void init(View view) {
        initResume();
    }




    @RequiresApi(api = Build.VERSION_CODES.N)
    private void initResume() {
        loading.setVisibility(View.VISIBLE);
       /* if (Build.VERSION.SDK_INT >= 23) {
            NumberFormat fmt = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
            currencyText = fmt.format(TotalGainParser.totalAmount);
        }else{*/
            currencyText = TotalGainParser.totalAmount;
//        }
        totalGain.setText("Total Gain: "+currencyText);
        PieChartPresenter pieChartPresenter = new PieChartImp(getActivity(), this);
        pieChartPresenter.getPieChartDataForIndex();
    }


    @Override
    public void indexCodeValue(ArrayList<IndexCodeModel> indexCodeModels, ArrayList<String> chartData, ArrayList<PieEntry> chartValue) {
        loading.setVisibility(View.GONE);
        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);

        pieChart.setDragDecelerationFrictionCoef(0.95f);

//        pieChart.setCenterTextTypeface(mTfLight);
//        pieChart.setCenterText(generateCenterSpannableText());

        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);

        pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setTransparentCircleAlpha(110);

        pieChart.setHoleRadius(58f);
        pieChart.setTransparentCircleRadius(61f);

        pieChart.setDrawCenterText(true);
        pieChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        pieChart.setRotationEnabled(true);
        pieChart.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
//        pieChart.setOnChartValueSelectedListener(this);


        pieChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        PieDataSet dataSet = new PieDataSet(chartValue, "");
        dataSet.setDrawIcons(false);
        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
//        data.setValueTypeface(mTfLight);
        pieChart.setData(data);
//        pieChart.getData().setDrawValues(false);
        pieChart.setDrawEntryLabels(false);
        // undo all highlights
        pieChart.highlightValues(null);

        pieChart.invalidate();
    }

    @Override
    public void companyTableData(ArrayList<String> headers, ArrayList<ArrayList<String>> contentsWithValue) {

    }

    @Override
    public void issueType(ArrayList<IssueTypeModelPie> issueTypeModelPies, ArrayList<String> chartData, ArrayList<PieEntry> chartValue) {

    }

    @Override
    public void symbolClicked(String s) {

    }

}
