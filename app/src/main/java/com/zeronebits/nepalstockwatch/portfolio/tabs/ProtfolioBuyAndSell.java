package com.zeronebits.nepalstockwatch.portfolio.tabs;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.inqbarna.tablefixheaders.TableFixHeaders;
import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyname.model.CompanyNameModel;
import com.zeronebits.nepalstockwatch.companyname.view.CompanyNameAdapter;
import com.zeronebits.nepalstockwatch.mainapplication.ApplicationMain;
import com.zeronebits.nepalstockwatch.portfolio.model.BuyStockModel;
import com.zeronebits.nepalstockwatch.portfolio.model.IssueTypeModel;
import com.zeronebits.nepalstockwatch.portfolio.model.SellStockModel;
import com.zeronebits.nepalstockwatch.portfolio.presenter.ProtfolioImp;
import com.zeronebits.nepalstockwatch.portfolio.presenter.ProtfolioPresenter;
import com.zeronebits.nepalstockwatch.portfolio.view.IssueTypeAdapter;
import com.zeronebits.nepalstockwatch.portfolio.view.PortfolioView;
import com.zeronebits.nepalstockwatch.portfolio.view.ProtfolioTableAdapter;
import com.zeronebits.nepalstockwatch.realmdatabase.RealmController;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Own on 4/11/2018.
 */

public class ProtfolioBuyAndSell extends BaseFragment implements PortfolioView {

      /* buy stock editText*/

    @BindView(R.id.et_buy_rate)
    EditText buyRate;
    @BindView(R.id.et_buy_units)
    EditText buyUnits;
    @BindView(R.id.btn_add_buy_stock)
    Button addBuyStock;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.act_company_name)
    AutoCompleteTextView companySymbol;
    CompanyNameAdapter companyNameAdapter;
    ArrayList<CompanyNameModel> companyNameModels;
    /* sell stock edittext */
    @BindView(R.id.et_sell_rate)
    EditText sellRate;
    @BindView(R.id.act_company_name_sell)
    AutoCompleteTextView sellStockSymbol;
    @BindView(R.id.et_sell_stock_volume)
    EditText sellVolume;
    @BindView(R.id.btn_sell_stock)
    Button sellStock;
    String companyName = "";
    MainActivity mainActivity;

    @BindView(R.id.table_bought_shares)
    TableFixHeaders tableSharedTrade;
    @BindView(R.id.table_sold_shares)
    TableFixHeaders soldTrade;

    @BindView(R.id.ll_sold_shares)
    LinearLayout soldShares;
    @BindView(R.id.ll_bought_shares)
    LinearLayout boughtShares;

    @BindView(R.id.v_sold)
    View viewSold;

    @BindView(R.id.v_bought)
    View viewBought;
    @BindView(R.id.avi)
    LinearLayout loading;
    String issueTypeCode;

    @BindView(R.id.ll_portfolio_buy)
    LinearLayout buyContent;
    @BindView(R.id.ll_portfolio_sell)
    LinearLayout sellContent;
    @BindView(R.id.ll_portfolio_buy_click)
    LinearLayout buyClick;
    @BindView(R.id.ll_portfolio_sell_click)
    LinearLayout sellClick;
    ProtfolioPresenter protfolioPresenter;

    @BindView(R.id.v_buy)
    View buyView;
    @BindView(R.id.v_sell)
    View sellView;


    @Override
    protected int getLayout() {
        return R.layout.sell_buy;
    }

    @Override
    protected void init(View view) {
        mainActivity = (MainActivity)getActivity();
        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        companyNameModels = realmController.getModelList();
        companySymbol.setThreshold(1);
        sellStockSymbol.setThreshold(1);
        companyNameAdapter = new CompanyNameAdapter(getActivity(), R.layout.simple_text_view, companyNameModels);
        companySymbol.setAdapter(companyNameAdapter);
        sellStockSymbol.setAdapter(companyNameAdapter);

        protfolioPresenter = new ProtfolioImp(this, getActivity());
        protfolioPresenter.getIssueType();
        boughtShares();
    }

    @OnClick(R.id.ll_bought_shares)
    public void boughtShares() {
        viewBought.setVisibility(View.VISIBLE);
        viewSold.setVisibility(View.GONE);
        soldTrade.setVisibility(View.GONE);
        tableSharedTrade.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.ll_sold_shares)
    public void soldShares() {
        viewBought.setVisibility(View.GONE);
        viewSold.setVisibility(View.VISIBLE);
        soldTrade.setVisibility(View.VISIBLE);
        tableSharedTrade.setVisibility(View.GONE);
    }


    @OnClick(R.id.ll_portfolio_sell_click)
    public void sellClick() {
        companyName = "";
        buyContent.setVisibility(View.GONE);
        sellContent.setVisibility(View.VISIBLE);
        sellView.setVisibility(View.VISIBLE);
        buyView.setVisibility(View.GONE);
    }

    @OnClick(R.id.ll_portfolio_buy_click)
    public void buyClick() {
        companyName = "";
        sellContent.setVisibility(View.GONE);
        buyContent.setVisibility(View.VISIBLE);
        sellView.setVisibility(View.GONE);
        buyView.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_add_buy_stock)
    public void BuyStock() {
        String buyRateString = buyRate.getText().toString();
        String buyVolumeString = buyUnits.getText().toString();
        companyName = companySymbol.getText().toString();

        if (buyRateString.length() == 0) {
            buyRate.setError("Required");
        } else if (buyVolumeString.length() == 0) {
            buyUnits.setError("Required");
        } else if (companyName.length() == 0) {
            Toast.makeText(getActivity(), "Company not selected", Toast.LENGTH_SHORT).show();
        } else {
            BuyStockModel buyStockModel = new BuyStockModel(Constant.USERCODE, buyVolumeString, buyRateString, issueTypeCode, companyName);
            protfolioPresenter.addBuyStock(buyStockModel);
        }
        boughtShares();
    }

    public void clearEditText() {
        buyUnits.setText("");
        buyRate.setText("");
        spinner.setSelection(0);
        companySymbol.setText("");
        sellRate.setText("");
        sellStockSymbol.setText("");
        sellVolume.setText("");
    }

    @OnClick(R.id.btn_sell_stock)
    public void sellStock() {
        String sellRateString = sellRate.getText().toString();
        companyName = sellStockSymbol.getText().toString();
        String sellVolumeString = sellVolume.getText().toString();

        if (sellRateString.length() == 0) {
            sellRate.setError("Required");
        } else if (sellVolumeString.length() == 0) {
            sellVolume.setError("Required");
        } else if (companyName.length() == 0) {
            Toast.makeText(getActivity(), "Company not selected", Toast.LENGTH_SHORT).show();
        } else {
            SellStockModel sellStockModel = new SellStockModel(Constant.USERCODE, sellVolumeString, sellRateString, companyName);
            protfolioPresenter.addSellStock(sellStockModel);
        }
        soldShares();
    }

    @Override
    public void issueTypeList(final ArrayList<IssueTypeModel> issueTypeModels) {
        IssueTypeAdapter issueTypeAdapter = new IssueTypeAdapter(getActivity(), R.layout.listitems_layout, issueTypeModels);
        spinner.setAdapter(issueTypeAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                issueTypeCode = issueTypeModels.get(position).getCODE();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        tableSharedTrade.setVisibility(View.GONE);
        soldTrade.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
        protfolioPresenter.getBoughtOrSoldShares();

    }

    @Override
    public void boughtAndSoldtableData(ArrayList<String> boughtHeadersArrayList, ArrayList<ArrayList<String>> boughtTableContentArrayStrings,
                                       ArrayList<String> soldHeadersArrayList, ArrayList<ArrayList<String>> soldTableContentArrayStrings) {
        loading.setVisibility(View.GONE);
        clearEditText();
//        tableSharedTrade.setVisibility(View.VISIBLE);
//        soldTrade.setVisibility(View.GONE);
        ProtfolioTableAdapter bouightTable = new ProtfolioTableAdapter(getActivity(), boughtHeadersArrayList, boughtTableContentArrayStrings);
        tableSharedTrade.setAdapter(bouightTable);

        ProtfolioTableAdapter soldTable = new ProtfolioTableAdapter(getActivity(), soldHeadersArrayList, soldTableContentArrayStrings);
        soldTrade.setAdapter(soldTable);
    }

    @Override
    public void addedSucessfully() {
        Toast.makeText(mainActivity, "Added successfully", Toast.LENGTH_SHORT).show();
//        tableSharedTrade.setVisibility(View.GONE);
//        soldTrade.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
        protfolioPresenter.getBoughtOrSoldShares();
    }

    @Override
    public void errorAdding() {
        Toast.makeText(mainActivity, "could not add", Toast.LENGTH_SHORT).show();
    }

}
