package com.zeronebits.nepalstockwatch.portfolio.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.data.PieEntry;
import com.inqbarna.tablefixheaders.TableFixHeaders;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyprofile.view.CompanyProfile;
import com.zeronebits.nepalstockwatch.dashboard.model.TotalGainParser;
import com.zeronebits.nepalstockwatch.dashboard.view.DashBoardFragment;
import com.zeronebits.nepalstockwatch.floorsheet.view.FloorSheetAdapter;
import com.zeronebits.nepalstockwatch.portfolio.tabs.model.IndexCodeModel;
import com.zeronebits.nepalstockwatch.portfolio.tabs.model.IssueTypeModelPie;
import com.zeronebits.nepalstockwatch.portfolio.tabs.presenter.PieChartImp;
import com.zeronebits.nepalstockwatch.portfolio.tabs.presenter.PieChartPresenter;
import com.zeronebits.nepalstockwatch.portfolio.view.PortfolioFragment;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Own on 11/9/2017.
 */


public class PortfolioByCompany extends BaseFragment implements PieChartView {


    @BindView(R.id.table)
    TableFixHeaders tableFixHeaders;
    CompanyPortfolioTableAdapter baseTableAdapter;
    @BindView(R.id.tv_total_gain)
    TextView totalGain;
    @BindView(R.id.avi)
    LinearLayout loading;

    @Override
    protected int getLayout() {
        return R.layout.portfolio_company_profile;
    }

    @Override
    protected void init(View view) {
        initResume();
    }


    private void initResume() {
        loading.setVisibility(View.VISIBLE);
        PieChartPresenter pieChartPresenter = new PieChartImp(getActivity(), this);
        pieChartPresenter.getPieChartDataForCompany();
        totalGain.setText("Total Gain: " + TotalGainParser.totalAmount);

        tableFixHeaders.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        tableFixHeaders.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }
                return false;
            }
        });

    }

    @Override
    public void indexCodeValue(ArrayList<IndexCodeModel> indexCodeModels, ArrayList<String> chartData, ArrayList<PieEntry> chartValue) {

    }

    @Override
    public void companyTableData(ArrayList<String> headers, ArrayList<ArrayList<String>> contentsWithValue) {
        Log.d("sjdhfdsfsdf", headers.size() + "   " + contentsWithValue.size());
        try {
            baseTableAdapter = new CompanyPortfolioTableAdapter(getActivity(), headers, contentsWithValue, this);
        } catch (Exception e) {

        }
        tableFixHeaders.setAdapter(baseTableAdapter);
        loading.setVisibility(View.GONE);
    }

    @Override
    public void issueType(ArrayList<IssueTypeModelPie> issueTypeModelPies, ArrayList<String> chartData, ArrayList<PieEntry> chartValue) {

    }

    @Override
    public void symbolClicked(String s) {
        try {
            DashBoardFragment dashBoardFragment = (DashBoardFragment) getParentFragment();
            dashBoardFragment.openFragment(s);
        } catch (Exception e) {
            PortfolioFragment portfolioFragment = (PortfolioFragment) getParentFragment();
            portfolioFragment.openFragment(s);
        }
    }

}
