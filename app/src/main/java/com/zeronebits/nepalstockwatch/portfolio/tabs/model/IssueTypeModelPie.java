package com.zeronebits.nepalstockwatch.portfolio.tabs.model;

/**
 * Created by Own on 11/14/2017.
 */

public class IssueTypeModelPie {

    public String issueType;
    String value;
    String units;

    public String getIssueType() {
        return issueType;
    }

    public void setIssueType(String issueType) {
        this.issueType = issueType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }
}
