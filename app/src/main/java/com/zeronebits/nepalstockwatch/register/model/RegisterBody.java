package com.zeronebits.nepalstockwatch.register.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Own on 10/15/2017.
 */

public class RegisterBody {

    @SerializedName("name")
    String username;
    @SerializedName("code")
    String mobile;
    @SerializedName("email")
    String email;
    @SerializedName("password")
    String password;


    public RegisterBody(String username, String mobile, String email, String password) {
        this.username = username;
        this.mobile = mobile;
        this.email = email;
        this.password = password;
    }
}
