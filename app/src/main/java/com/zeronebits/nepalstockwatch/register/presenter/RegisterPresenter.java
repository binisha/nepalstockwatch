package com.zeronebits.nepalstockwatch.register.presenter;

import com.zeronebits.nepalstockwatch.register.model.RegisterBody;

/**
 * Created by Own on 10/15/2017.
 */

public interface RegisterPresenter{
    void registerUser(RegisterBody registerBody);
}
