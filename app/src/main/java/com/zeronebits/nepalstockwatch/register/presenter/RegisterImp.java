package com.zeronebits.nepalstockwatch.register.presenter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.floorsheet.model.FloorSheetParser;
import com.zeronebits.nepalstockwatch.helper.LoadingHelper;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.login.view.LoginActivity;
import com.zeronebits.nepalstockwatch.register.model.RegisterBody;
import com.zeronebits.nepalstockwatch.register.view.RegisterView;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;
import com.zeronebits.nepalstockwatch.verification.view.VerificationActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 10/15/2017.
 */

public class RegisterImp implements RegisterPresenter{

    RegisterView registerView;
    Context context;


    public RegisterImp(RegisterView registerView, Context context) {
        this.registerView = registerView;
        this.context = context;
    }


    @Override
    public void registerUser(RegisterBody registerBody) {

        final LoadingHelper loadingHelper = new LoadingHelper(context);
        loadingHelper.showDialog();
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();
        Log.d("CheckingCountryResponse", "i am here");

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.createUser(registerBody);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loadingHelper.disMiss();
                Log.d("djsfdsfdsf",response.code() + "");
                if(response.isSuccessful()){
                    Intent i = new Intent(context, VerificationActivity.class);
                    context.startActivity(i);
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    try {
                        Constant.showJsonErrorMsgNewArray(context,response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                /* catch (JSONException e) {
                    e.printStackTrace();
                }
*/
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loadingHelper.disMiss();
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }
}
