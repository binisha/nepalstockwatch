package com.zeronebits.nepalstockwatch.userupdate.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Own on 1/31/2018.
 */

public class SecurityQuestionParser {

    String result;
    public ArrayList<SecurityQuestionModel> securityQuestionModels;

    public SecurityQuestionParser(String result) {
        this.result = result;
        securityQuestionModels = new ArrayList<>();
    }

    public void parser() throws JSONException {
        JSONArray json = new JSONArray(result);
        for(int i = 0; i< json.length(); i++){
            JSONObject jsonObject = json.getJSONObject(i);
            SecurityQuestionModel securityQuestionModel = new SecurityQuestionModel();
            securityQuestionModel.setCode(jsonObject.getInt("CODE"));
            securityQuestionModel.setQuestion(jsonObject.getString("QUESTION"));
            securityQuestionModels.add(securityQuestionModel);
        }
    }
}
