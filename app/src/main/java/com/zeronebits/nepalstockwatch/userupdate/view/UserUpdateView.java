package com.zeronebits.nepalstockwatch.userupdate.view;

import com.zeronebits.nepalstockwatch.userupdate.model.SecurityQuestionModel;

import java.util.ArrayList;

/**
 * Created by Own on 1/23/2018.
 */

public interface UserUpdateView {
    void passwordChanged(String message);

    void securityData(ArrayList<SecurityQuestionModel> securityQuestionModels);

    void updateSucess(String message);
}
