package com.zeronebits.nepalstockwatch.userupdate.view;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.portfolio.model.IssueTypeModel;
import com.zeronebits.nepalstockwatch.portfolio.view.IssueTypeAdapter;
import com.zeronebits.nepalstockwatch.userupdate.model.SecurityQuestionModel;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

/**
 * Created by Own on 1/31/2018.
 */

public class SecurityQuestionAdapter extends ArrayAdapter<SecurityQuestionModel> {

    LayoutInflater flater;
    Context context;

    public SecurityQuestionAdapter(Activity context, int resouceId, ArrayList<SecurityQuestionModel> list) {
        super(context, resouceId, list);
        this.context = context;
//        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return rowview(convertView, position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return rowview(convertView, position);
    }

    private View rowview(View convertView, int position) {

        SecurityQuestionModel rowItem = getItem(position);

        viewHolder holder;
        View rowview = convertView;
        if (rowview == null) {

            holder = new viewHolder();
            flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = flater.inflate(R.layout.listitems_layout, null, false);

            holder.txtTitle = (TextView) rowview.findViewById(R.id.tv_message_header);
            rowview.setTag(holder);
        } else {
            holder = (viewHolder) rowview.getTag();
        }
        holder.txtTitle.setText(rowItem.getQuestion());
        holder.txtTitle.setTextColor(ContextCompat.getColor(context
        ,R.color.blue_text));
        return rowview;
    }

    private class viewHolder {
        TextView txtTitle;
    }
}