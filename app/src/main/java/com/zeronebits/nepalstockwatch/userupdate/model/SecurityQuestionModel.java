package com.zeronebits.nepalstockwatch.userupdate.model;

/**
 * Created by Own on 1/31/2018.
 */

public class SecurityQuestionModel {

    int code;
    String question;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
