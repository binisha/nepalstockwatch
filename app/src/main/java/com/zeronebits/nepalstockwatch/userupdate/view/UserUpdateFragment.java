package com.zeronebits.nepalstockwatch.userupdate.view;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.login.model.UserModelForUpdate;
import com.zeronebits.nepalstockwatch.login.model.UserResponseModel;
import com.zeronebits.nepalstockwatch.splash.model.SplashParser;
import com.zeronebits.nepalstockwatch.userupdate.model.SecurityQuestionModel;
import com.zeronebits.nepalstockwatch.userupdate.presenter.UserUpdateImp;
import com.zeronebits.nepalstockwatch.userupdate.presenter.UserUpdatePresenter;
import com.zeronebits.nepalstockwatch.utils.CircleTransform;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalwatchstock.R;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Own on 1/23/2018.
 */

public class UserUpdateFragment extends BaseFragment implements UserUpdateView {

    MainActivity mainActivity;
    @BindView(R.id.et_full_name)
    EditText etFullName;
    @BindView(R.id.et_email_address)
    EditText etEmail;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_phone_number_primary)
    EditText etPhonePrimary;
    @BindView(R.id.et_phone_number_secondary)
    EditText etPhoneSecondary;
    @BindView(R.id.et_answer)
    EditText etAnswer;
    @BindView(R.id.et_pin)
    EditText etPin;
    @BindView(R.id.et_pin_confirm)
    EditText etPinConfirm;
    @BindView(R.id.et_password)
    EditText etNewPassword;
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;
    @BindView(R.id.et_old_password)
    EditText etOldPassword;
    @BindView(R.id.sp_security_question)
    Spinner securityQuestionSpinner;
    UserUpdatePresenter userUpdatePresenter;
    UserResponseModel userResponseModel;
    int securityQuestion=0;
    @BindView(R.id.iv_user_profile)
    ImageView userProfile;
    private String imagepath = "";
    String fileName;
    private String fullName,email,address,phoneSecondary,answer;

    @Override
    protected int getLayout() {
        return R.layout.user_update;
    }

    @Override
    protected void init(View view) {
        userUpdatePresenter = new UserUpdateImp(getActivity(), this);
        userUpdatePresenter.getSecurityData();
    }

    private String getEditTextValue(EditText et) {
        return et.getText().toString();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mainActivity = (MainActivity) getActivity();
            mainActivity.setToolBarTitle("Settings");
        } catch (Exception e) {

        }
    }

    @OnClick(R.id.btn_change_pass)
    public void changePassword() {
        String oldPassword = getEditTextValue(etOldPassword).trim();
        String newPassword = getEditTextValue(etNewPassword).trim();
        String confirmNewPassword = getEditTextValue(etConfirmPassword).trim();

        if (oldPassword.isEmpty()) {
            etOldPassword.setError("Required");
        } else if (newPassword.isEmpty()) {
            etNewPassword.setError("Required");
        } else if (confirmNewPassword.isEmpty()) {
            etConfirmPassword.setError("Required");
        } else if (!confirmNewPassword.equalsIgnoreCase(newPassword)) {
            etConfirmPassword.setError("Incorrect Password");
        } else {
            userUpdatePresenter.changePassword(oldPassword, newPassword, confirmNewPassword);
        }
    }

    @OnClick(R.id.iv_user_profile)
    public void userProfile() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(getActivity(), UserUpdateFragment.this);
    }


    @OnClick(R.id.btn_submit)
    public void submitLogin() {
        fullName = getEditTextValue(etFullName);
        email = getEditTextValue(etEmail);
        address = getEditTextValue(etAddress);
        phoneSecondary = getEditTextValue(etPhoneSecondary);
        answer = getEditTextValue(etAnswer);

        Log.d("checkingaksd", fullName + "  "+email);
        UserModelForUpdate userModelForUpdate = new UserModelForUpdate(Constant.USERCODE, 0, 0,
                fullName, fileName, address, email, "", phoneSecondary, "", securityQuestion, answer);
        userUpdatePresenter.updateUser(userModelForUpdate, imagepath);

    }


    public void setDataToField() {
        userResponseModel = SplashParser.userResponseModel;
        Log.d("dskjhfdsgfgfdg", userResponseModel.getnAME() + "   ggfhgfh");
        fullName = userResponseModel.getnAME();
        email = userResponseModel.geteMAIL();
        phoneSecondary = userResponseModel.getaLTPHONE();
        address = userResponseModel.getaDDR();
        etFullName.setText(userResponseModel.getnAME());
        etEmail.setText(userResponseModel.geteMAIL());
        etAddress.setText(userResponseModel.getaDDR());
        etPhonePrimary.setText(userResponseModel.getcODE() + "");
        etPhoneSecondary.setText(userResponseModel.getaLTPHONE());
        if (userResponseModel.getpHOTOURL().equalsIgnoreCase("")) {
            setUserProfilePic(userResponseModel.getnAME());
        } else {
            Picasso.with(getActivity()).load(userResponseModel.getpHOTOURL()).transform(new CircleTransform()).into(userProfile);
        }
//        securityQuestion = userResponseModel.getsQCODE();
        etAnswer.setText(userResponseModel.getsQANS());
        securityQuestionSpinner.setSelection( userResponseModel.getsQCODE());
//        etSecurity.setText(userResponseModel.getnAME());
//        etFullName.setText(userResponseModel.getnAME());
//        etFullName.setText(userResponseModel.getnAME());
    }

    @Override
    public void passwordChanged(String message) {
        Toast.makeText(mainActivity, message, Toast.LENGTH_SHORT).show();
        etOldPassword.setText("");
        etNewPassword.setText("");
        etConfirmPassword.setText("");
    }

    @Override
    public void securityData(final ArrayList<SecurityQuestionModel> securityQuestionModels) {
        setDataToField();
        SecurityQuestionAdapter issueTypeAdapter = new SecurityQuestionAdapter(getActivity(), R.layout.listitems_layout, securityQuestionModels);
        securityQuestionSpinner.setAdapter(issueTypeAdapter);
        securityQuestionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                securityQuestion = securityQuestionModels.get(position).getCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void updateSucess(String message) {
        Toast.makeText(mainActivity, message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                final InputStream imageStream;
                try {
                    imageStream = getActivity().getContentResolver().openInputStream(result.getUri());
                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    Bitmap resizedImage = Bitmap.createScaledBitmap(selectedImage, 400, 400, true);
                    String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), resizedImage, "Title", null);
                    Bitmap circleImage = Constant.changeImageToCircle(selectedImage);
                    userProfile.setImageBitmap(circleImage);
//                    encodeImage(resizedImage);
                    imagepath = getRealPathFromURI(Uri.parse(path));
                    String name = ("niroj");
                    String tag = name + ".jpg";
                    fileName = imagepath.replace(imagepath, tag);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(getActivity(), "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }


    public String getRealPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            ;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    public void setUserProfilePic(String name) {
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
// generate random color
// generate color based on a key (same key returns the same color), useful for list/grid views
        int color2 = generator.getColor(name);
// declare the builder object once.
        TextDrawable.IBuilder builder = TextDrawable.builder()
                .beginConfig()
                .fontSize(50)
                .endConfig()
                .round();

// reuse the builder specs to create multiple drawables
        TextDrawable ic2 = builder.build(name.substring(0, 1).toUpperCase(), color2);
        userProfile.setImageDrawable(ic2);
    }

}
