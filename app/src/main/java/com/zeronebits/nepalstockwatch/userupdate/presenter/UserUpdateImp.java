package com.zeronebits.nepalstockwatch.userupdate.presenter;

import android.content.Context;
import android.util.Log;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.helper.LoadingHelper;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.login.model.UserModelForUpdate;
import com.zeronebits.nepalstockwatch.splash.model.SplashParser;
import com.zeronebits.nepalstockwatch.userupdate.model.SecurityQuestionParser;
import com.zeronebits.nepalstockwatch.userupdate.view.UserUpdateView;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import io.realm.CompactOnLaunchCallback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import rx.internal.operators.CompletableOnSubscribeConcat;

/**
 * Created by Own on 1/23/2018.
 */

public class UserUpdateImp implements UserUpdatePresenter {

    Context context;
    UserUpdateView userUpdateView;

    public UserUpdateImp(Context context, UserUpdateView userUpdateView) {
        this.context = context;
        this.userUpdateView = userUpdateView;
    }

    @Override
    public void updateUser(UserModelForUpdate userModelForUpdate,String imagePath) {
        final LoadingHelper loadingHelper = new LoadingHelper(context);
        loadingHelper.showDialog();
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry;
        //File creating from selected URL
        if(imagePath.equalsIgnoreCase("")){
           getCountry = loginRegInterface.updateUserDetailWithoutBody(userModelForUpdate,Constant.USERCODE);
        }else {
            File file = new File(imagePath);

            // create RequestBody instance from file
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("PHOTO_URL", file.getName(), requestFile);
          getCountry = loginRegInterface.updateUserDetail(body, userModelForUpdate,Constant.USERCODE);

        }
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("sdkfjdsfsdf", response.code() + "  ");
                Log.d("sdkfjdsfsdffdg",  bodyToString(response.raw().request().body()));
                loadingHelper.disMiss();
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        SplashParser loginParser = new SplashParser(result);
                        loginParser.parser();
                        userUpdateView.updateSucess(new JSONObject(result).getString("message"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else if(response.code() == 401 || response.code() == 409){
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

//                    loginView.sendVerification();
                } else {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
//                        loginView.failedLogin();
                    }catch (Exception e){

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void changePassword(String oldPassword, String newPassword, String confirmNewPassword) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.changePassword(Constant.USERCODE,
                oldPassword,newPassword,confirmNewPassword);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse",response.code()+"    "+ response.raw().request().url() + "  ");
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String message = jsonObject.getString("message");
                        userUpdateView.passwordChanged(message);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else if(response.code() == 401 || response.code() == 409){
//                    loginView.sendVerification();
                } else {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
//                        loginView.failedLogin();
                    }catch (Exception e){

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getSecurityData() {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getSecurityData();
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse",response.code()+"    "+ response.raw().request().url() + "  ");
                if (response.isSuccessful()) {
                    try {
                        SecurityQuestionParser securityQuestionParser = new SecurityQuestionParser(response.body().string());
                        securityQuestionParser.parser();
                        userUpdateView.securityData(securityQuestionParser.securityQuestionModels);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else if(response.code() == 401 || response.code() == 409){
//                    loginView.sendVerification();
                } else {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
//                        loginView.failedLogin();
                    }catch (Exception e){

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    private String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}
