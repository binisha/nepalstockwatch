package com.zeronebits.nepalstockwatch.dashboard.view;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.data.Entry;
import com.inqbarna.tablefixheaders.TableFixHeaders;
import com.inqbarna.tablefixheaders.adapters.BaseTableAdapter;
import com.wang.avi.AVLoadingIndicatorView;
import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyprofile.view.CompanyProfile;
import com.zeronebits.nepalstockwatch.dashboard.model.MarketSummaryModel;
import com.zeronebits.nepalstockwatch.dashboard.presentar.DashBoardTableImplementar;
import com.zeronebits.nepalstockwatch.dashboard.presentar.DashBoardTablePresenter;
import com.zeronebits.nepalstockwatch.dashboard.tabs.view.Company;
import com.zeronebits.nepalstockwatch.dashboard.tabs.view.Index;
import com.zeronebits.nepalstockwatch.dashboard.tabs.view.PortfolioByPortfolio;
import com.zeronebits.nepalstockwatch.dashboard.tabs.view.SubIndex;
import com.zeronebits.nepalstockwatch.mainactivitynotlogin.view.MainActivityNotLogin;
import com.zeronebits.nepalstockwatch.portfolio.tabs.PortfolioByCompany;
import com.zeronebits.nepalstockwatch.portfolio.tabs.PortfolioByIndex;
import com.zeronebits.nepalstockwatch.portfolio.tabs.PortfolioByIssues;
import com.zeronebits.nepalstockwatch.portfolio.view.PorfolioTabAdapter;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.WrapContentViewPager;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Own on 8/19/2017.
 */

public class DashBoardFragment extends BaseFragment implements DashBoardView {

    public DashBoardTablePresenter dashBoardTablePresenter;
    @BindView(R.id.table_transaction)
    TableFixHeaders tableTransaction;
    @BindView(R.id.table_shared_trade)
    TableFixHeaders tableSharedTrade;
    @BindView(R.id.table_turn_over)
    TableFixHeaders tableTurnOver;
    @BindView(R.id.table_gain)
    TableFixHeaders tableGain;
    @BindView(R.id.table_loss)
    TableFixHeaders tableLoss;
    @BindView(R.id.avi)
    public AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.ll_dashboard_table)
    LinearLayout dashboardTable;

    MainActivity mainActivity;
    MainActivityNotLogin mainActivityNotLogin;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.pager)
    ViewPager viewPager;
    @BindView(R.id.ll_market_summary)
    LinearLayout marketSummary;
    int month = 1;
    String selectedIndex;
    Handler handler, handler1, handler2, handler3, handler4;
    Runnable runnable, runnable1, runnable2, runnable3;

    @Override
    protected int getLayout() {
       /* if (Constant.ACESS_TOKEN.equalsIgnoreCase("")) {
            return R.layout.dashboard;
        }else{*/
        return R.layout.dashboard_login_new;

//        }
    }

    @Override
    protected void init(View view) {
        handler = new Handler();
        handler1 = new Handler();
        handler2 = new Handler();
        handler3 = new Handler();
        dashBoardTablePresenter = new DashBoardTableImplementar(this, getActivity());
        dashBoardTablePresenter.getAllDashboardData();
        tabs.setupWithViewPager(viewPager);
        Log.d("sdlfjsdfdsfdsf", Constant.ACESS_TOKEN + " dsdsd");
        if (Constant.ACESS_TOKEN.equalsIgnoreCase("")) {
//            viewPager.setOffscreenPageLimit(2);
            createViewPagerGuest(viewPager);
            createTabIconsGuest();
        } else {
            createViewPagerLogin(viewPager);
            createTabIconsLogin();
          /*  loadingHelper = new LoadingHelper(getActivity());
            loadingHelper.showDialog();*/
//            viewPager.setOffscreenPageLimit(3);
        }
        hideKeyboard();
        marketSummary.setVisibility(View.GONE);

    }


    @Override
    public void onResume() {
        super.onResume();

//        dashboardTable.setVisibility(View.GONE);
        try {
            mainActivity = (MainActivity) getActivity();
            mainActivity.setToolBarTitle("Dashboard");
        } catch (Exception e) {
            mainActivityNotLogin = (MainActivityNotLogin) getActivity();
        }
    }

    void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = getActivity().getCurrentFocus();

        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    private void createViewPagerGuest(ViewPager viewPager) {
        PorfolioTabAdapter adapter = new PorfolioTabAdapter(getChildFragmentManager());
        adapter.addFrag(new Index(), "Index");
        adapter.addFrag(new SubIndex(), "SubIndex");
        adapter.addFrag(new Company(), "Company");
        viewPager.setAdapter(adapter);

    }

    private void createTabIconsGuest() {

        TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabOne.setText("Index");
        tabs.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabTwo.setText("SubIndex");
        tabs.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabThree.setText("Company");
        tabs.getTabAt(2).setCustomView(tabThree);
    }

    private void createViewPagerLogin(ViewPager viewPager) {
        PorfolioTabAdapter adapter = new PorfolioTabAdapter(getChildFragmentManager());
        adapter.addFrag(new PortfolioByPortfolio(), "Portfolio");
        adapter.addFrag(new PortfolioByCompany(), "Company");
        adapter.addFrag(new PortfolioByIndex(), "Index");
        adapter.addFrag(new PortfolioByIssues(), "Issue Type");
        viewPager.setAdapter(adapter);

    }

    private void createTabIconsLogin() {

        TextView tabFirst = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabFirst.setText("Portfolio");
        tabs.getTabAt(0).setCustomView(tabFirst);

        TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabOne.setText("Company");
        tabs.getTabAt(1).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabTwo.setText("Index");
        tabs.getTabAt(2).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabThree.setText("Issue Type");
        tabs.getTabAt(3).setCustomView(tabThree);
    }


    @Override
    public void setLossAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList) {
        BaseTableAdapter baseTableAdapter = new DashBoardTableAdapter(getActivity(), headers, tableContentsList);
        tableLoss.setAdapter(baseTableAdapter);

        loadingIndicatorView.setVisibility(View.GONE);
        dashboardTable.setVisibility(View.VISIBLE);
    }

    @Override
    public void setGainAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList) {
        BaseTableAdapter baseTableAdapter = new DashBoardTableAdapter(getActivity(), headers, tableContentsList);
        tableGain.setAdapter(baseTableAdapter);
        dashBoardTablePresenter.getLoss();

        loadingIndicatorView.setVisibility(View.GONE);
        dashboardTable.setVisibility(View.VISIBLE);
    }

    @Override
    public void setTransactionAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList) {
        BaseTableAdapter baseTableAdapter = new DashBoardTableAdapter(getActivity(), headers, tableContentsList);
        tableTransaction.setAdapter(baseTableAdapter);
        dashBoardTablePresenter.getShareTraded();

        loadingIndicatorView.setVisibility(View.GONE);
        dashboardTable.setVisibility(View.VISIBLE);
    }

    @Override
    public void setSharedTradedAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList) {
        BaseTableAdapter baseTableAdapter = new DashBoardTableAdapter(getActivity(), headers, tableContentsList);
        tableSharedTrade.setAdapter(baseTableAdapter);
        dashBoardTablePresenter.getGain();

        loadingIndicatorView.setVisibility(View.GONE);
        dashboardTable.setVisibility(View.VISIBLE);
    }

    @Override
    public void setTurnedOverAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList) {
        BaseTableAdapter baseTableAdapter = new DashBoardTableAdapter(getActivity(), headers, tableContentsList);
        tableTurnOver.setAdapter(baseTableAdapter);
        dashBoardTablePresenter.getGain();

        loadingIndicatorView.setVisibility(View.GONE);
        dashboardTable.setVisibility(View.VISIBLE);
    }

    @Override
    public void dashboardTableData(final ArrayList<String> turnOVerHeadersArrayList,
                                   final ArrayList<ArrayList<String>> turnOverTableContentArrayStrings,
                                   final ArrayList<String> shareHeadersArrayList, final ArrayList<ArrayList<String>>
                                           shareTableContentArrayStrings, final ArrayList<String> gainHeadersArrayList,
                                   final ArrayList<ArrayList<String>> gainTableContentArrayStrings, final ArrayList<String> lossHeadersArrayList,
                                   final ArrayList<ArrayList<String>> lossTableContentArrayStrings,
                                   MarketSummaryModel marketSummaryModel) {
        loadingIndicatorView.setVisibility(View.GONE);
        dashboardTable.setVisibility(View.VISIBLE);
        tableTransaction.setVisibility(View.GONE);
        runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    BaseTableAdapter lossBaseTableAdapter = new DashBoardTableAdapter(getActivity(), lossHeadersArrayList, lossTableContentArrayStrings);
                    tableLoss.setAdapter(lossBaseTableAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        runnable1 = new Runnable() {
            @Override
            public void run() {
                try {
                    BaseTableAdapter gainBaseTableAdapter = new DashBoardTableAdapter(getActivity(), gainHeadersArrayList, gainTableContentArrayStrings);
                    tableGain.setAdapter(gainBaseTableAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };


        runnable2 = new Runnable() {
            @Override
            public void run() {
                try {
                    BaseTableAdapter shareTableAdapter = new DashBoardTableAdapter(getActivity(), shareHeadersArrayList, shareTableContentArrayStrings);
                    tableSharedTrade.setAdapter(shareTableAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        runnable3 = new Runnable() {
            @Override
            public void run() {
                try {
                    BaseTableAdapter turnTableAdapter = new DashBoardTableAdapter(getActivity(), turnOVerHeadersArrayList, turnOverTableContentArrayStrings);
                    tableTurnOver.setAdapter(turnTableAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 4 * 1000);
        handler1.postDelayed(runnable1, 3 * 1000);
        handler2.postDelayed(runnable2, 2 * 1000);
        handler3.postDelayed(runnable3, 1 * 1000);

    }

    @Override
    public void onPause() {
        super.onPause();
        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
        if (handler1 != null) {
            handler1.removeCallbacks(runnable1);
        }
        if (handler2 != null) {
            handler2.removeCallbacks(runnable2);
        }
        if (handler3 != null) {
            handler3.removeCallbacks(runnable3);
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void openFragment(String s) {
        Fragment fragment = new CompanyProfile();
        Bundle b = new Bundle();
        b.putString(Constant.COMPANYCODE, s);
        fragment.setArguments(b);
        getFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame, fragment).addToBackStack("tag").commit();
    }
}