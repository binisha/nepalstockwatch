package com.zeronebits.nepalstockwatch.dashboard.model;

import android.util.Log;

import com.github.mikephil.charting.data.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Own on 11/9/2017.
 */

public class DashboardChartParser  {

    String result;
    public ArrayList<String> chartData;
    public ArrayList<Entry> chartValue;

    public DashboardChartParser(String result) {
        this.result = result;
        chartData= new ArrayList<>();
        chartValue = new ArrayList<>();
    }
    public void parser() throws JSONException {
        JSONArray jsonArray = new JSONArray(result);
        Log.d("ksdfsdfdf", result + "   sdfdfsdf");
        if(jsonArray.length() == 0){

        }else {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject data = jsonArray.getJSONObject(i);
                DashboardChartDataModel dashboardChartDataModel = new DashboardChartDataModel();
                dashboardChartDataModel.setaBSCHANGE(data.getString("ABS_CHANGE"));
                dashboardChartDataModel.setdATE(data.getString("DATE"));
                dashboardChartDataModel.setiNDEXCODE(data.getString("INDEX_CODE"));
                dashboardChartDataModel.settURNOVER(data.getString("TURNOVER"));
                dashboardChartDataModel.setvALUE(Float.parseFloat(data.getString("VALUE")));
                dashboardChartDataModel.setpCTCHANGE(data.getString("PCT_CHANGE"));
                chartData.add(convertToApiFormat(data.getString("DATE")));
                chartValue.add(new Entry(Float.parseFloat(i + ""), Float.parseFloat(data.getString("VALUE"))));
            }
        }
    }


    public String convertToApiFormat(String time) {
        String inputPattern = "yyyy-mm-dd";
        String outputPattern = "dd-MMM";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d(str, "checkdate");
        return str;
    }
}
