package com.zeronebits.nepalstockwatch.dashboard.model;

import java.util.ArrayList;

/**
 * Created by Own on 9/1/2017.
 */

public class DashBoardTableModel {
    public ArrayList<String> headers;
    public ArrayList<String> contents;

    public ArrayList<String> getHeaders() {
        return headers;
    }

    public void setHeaders(ArrayList<String> headers) {
        this.headers = headers;
    }

    public ArrayList<String> getContents() {
        return contents;
    }

    public void setContents(ArrayList<String> contents) {
        this.contents = contents;
    }
}
