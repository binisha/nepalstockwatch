package com.zeronebits.nepalstockwatch.dashboard.tabs.presenter;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companyhistory.model.CompanyDataParser;
import com.zeronebits.nepalstockwatch.dashboard.model.DashboardChartParser;
import com.zeronebits.nepalstockwatch.dashboard.model.DashboardPortfolioParser;
import com.zeronebits.nepalstockwatch.dashboard.model.TotalGainParser;
import com.zeronebits.nepalstockwatch.dashboard.tabs.view.LineChartView;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 11/13/2017.
 */

public class LineChartImp implements LineChartPresenter {

    LineChartView lineChartView;
    Context context;


    public LineChartImp(LineChartView lineChartView, Context context) {
        this.lineChartView = lineChartView;
        this.context = context;
    }

    @Override
    public void getGraphData(String selectedIndex, int month) {

        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getDashboardChartData(selectedIndex, month);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCesponse", response.raw().request().url() + "  ");

                if(response.isSuccessful()) {
                    try {
                        DashboardChartParser dashboardChartParser = new DashboardChartParser(response.body().string());
                        dashboardChartParser.parser();
                        if(dashboardChartParser.chartData.size() == 0){
                            lineChartView.nodDataFound();
                        }else {
                            lineChartView.setGraphValue(dashboardChartParser.chartValue, dashboardChartParser.chartData);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(context, "Could not load data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                loadingHelper.disMiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getPortfolioData(int month) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getProtfolio(Constant.USERCODE, month);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingRespjbbonse", response.code() + "  "+response.raw().request().url());
                if(response.isSuccessful()) {
                    try {
                        DashboardPortfolioParser dashboardPortfolioParser = new DashboardPortfolioParser(response.body().string());
                        dashboardPortfolioParser.parser();
                        lineChartView.setPortfolioResponse(dashboardPortfolioParser.chartData, dashboardPortfolioParser.chartValue);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    lineChartView.nodDataFound();
                    Toast.makeText(context, "Could not load data", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }


    @Override
    public void getTotalAmount(String usercode) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getTotalGain(usercode);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("totalGain", response.raw().request().url() + "  ");
                if (response.isSuccessful()) {
                    try {
                        TotalGainParser totalGainParser = new TotalGainParser(response.body().string());
                        totalGainParser.parser();
                        lineChartView.totalAmountSucess();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(context, "Could not load data", Toast.LENGTH_SHORT).show();

                        Log.d("sdkfjsdfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }



    @Override
    public void getPortfolioList(int i) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getProtfolio(Constant.USERCODE, i);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingRespjbbonse", response.code() + "  "+response.raw().request().url());
                if(response.isSuccessful()) {
                    try {
                        DashboardPortfolioParser dashboardPortfolioParser = new DashboardPortfolioParser(response.body().string());
                        dashboardPortfolioParser.parser();
                        lineChartView.setPortfolioResponse(dashboardPortfolioParser.chartData, dashboardPortfolioParser.chartValue);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    lineChartView.nodDataFound();
                    try {
                        Toast.makeText(context, "Could not load data", Toast.LENGTH_SHORT).show();

                        Log.d("asdasdsadasd",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getGraphDataForCompany(String selectedIndex, int month) {

        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getCompanyData(selectedIndex, month);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("jsdfdsbfdsf", response.raw().request().url() + "  "+response.code());

                if(response.isSuccessful()) {
                    try {
                        CompanyDataParser companyDataParser = new CompanyDataParser(response.body().string());
                        companyDataParser.parser();
                        Log.d("jsdfdsbfdsf",companyDataParser.contentsWithValue.size() +"    null");
                        if(companyDataParser.contentsWithValue.size() == 0){
                            lineChartView.nodDataFound();
                        }else {
                            lineChartView.setGraphValue(companyDataParser.chartValue, companyDataParser.chartData);
                            lineChartView.setTableValue(companyDataParser.headers, companyDataParser.contentsWithValue);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(context, "Could not load data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                loadingHelper.disMiss();
                t.printStackTrace();
            }
        });
    }
}

