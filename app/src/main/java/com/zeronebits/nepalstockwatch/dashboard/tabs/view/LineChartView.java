package com.zeronebits.nepalstockwatch.dashboard.tabs.view;

import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;

/**
 * Created by Own on 11/13/2017.
 */

public interface LineChartView {
    void setGraphValue(ArrayList<Entry> chartValue, ArrayList<String> chartData);

    void nodDataFound();

    void setPortfolioResponse(ArrayList<String> chartData, ArrayList<Entry> chartValue);

    void totalAmountSucess();

    void setTableValue(ArrayList<String> headers, ArrayList<ArrayList<String>> contentsWithValue);
}
