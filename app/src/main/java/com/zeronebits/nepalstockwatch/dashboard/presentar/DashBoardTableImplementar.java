package com.zeronebits.nepalstockwatch.dashboard.presentar;

import android.content.Context;
import android.util.Log;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.dashboard.model.DashBoardTableParser;
import com.zeronebits.nepalstockwatch.dashboard.model.DashboardChartParser;
import com.zeronebits.nepalstockwatch.dashboard.model.DashboardPortfolioParser;
import com.zeronebits.nepalstockwatch.dashboard.model.TotalGainParser;
import com.zeronebits.nepalstockwatch.dashboard.view.DashBoardView;
import com.zeronebits.nepalstockwatch.helper.LoadingHelper;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 9/1/2017.
 */

public class DashBoardTableImplementar implements DashBoardTablePresenter {

    DashBoardView dashBoardView;
    ArrayList<ArrayList<String>> tableContentsList;
    ArrayList<String> headers;
    Context context;

    public DashBoardTableImplementar(DashBoardView dashBoardView , Context context) {
        this.dashBoardView = dashBoardView;
        this.context = context;
    }


    @Override
    public void getAllDashboardData() {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getAllDashboardData();
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("Checkiponsesdfdsf", response.code() + "  ");

                if(response.isSuccessful()) {
                    try {
                        DashBoardTableParser dashBoardTableParser = new DashBoardTableParser(response.body().string());
                        dashBoardTableParser.parser();
                        Log.d("sdkjfsdklfjdkf", "fdfher");

                        dashBoardView.dashboardTableData(dashBoardTableParser.turnOVerHeadersArrayList, dashBoardTableParser.turnOverTableContentArrayStrings,
                                dashBoardTableParser.shareHeadersArrayList, dashBoardTableParser.shareTableContentArrayStrings,
                                dashBoardTableParser.gainHeadersArrayList, dashBoardTableParser.gainTableContentArrayStrings,
                                dashBoardTableParser.lossHeadersArrayList, dashBoardTableParser.lossTableContentArrayStrings,
                                dashBoardTableParser.marketSummaryModel);
//                        dashBoardView.setTurnedOverAdapter(headers, tableContentsList);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getTurnedOver() {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.ApiGetTurnedOver();
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  ");
                try {
                    DashBoardTableParser dashBoardTableParser = new DashBoardTableParser(response.body().string());
                    dashBoardTableParser.parse();
                    tableContentsList = dashBoardTableParser.getTableContentArrayStrings();
                    headers = dashBoardTableParser.getHeadersArrayList();

                    Log.d("sdkjfsdklfjdkf", tableContentsList.size() + "   " + headers.size());
                    dashBoardView.setTurnedOverAdapter(headers, tableContentsList);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getShareTraded() {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.ApiGetShareTraded();
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  ");
                try {
                    DashBoardTableParser dashBoardTableParser = new DashBoardTableParser(response.body().string());
                    dashBoardTableParser.parse();
                    tableContentsList = dashBoardTableParser.getTableContentArrayStrings();
                    headers = dashBoardTableParser.getHeadersArrayList();

                    Log.d("sdkjfsdklfjdkf", tableContentsList.size() + "   " + headers.size());
                    dashBoardView.setSharedTradedAdapter(headers, tableContentsList);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getNoOfTransaction() {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.ApiGetNoOfTransaction();
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  ");
                try {
                    DashBoardTableParser dashBoardTableParser = new DashBoardTableParser(response.body().string());
                    dashBoardTableParser.parse();
                    tableContentsList = dashBoardTableParser.getTableContentArrayStrings();
                    headers = dashBoardTableParser.getHeadersArrayList();

                    Log.d("sdkjfsdklfjdkf", tableContentsList.size() + "   " + headers.size());
                    dashBoardView.setTransactionAdapter(headers, tableContentsList);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getGain() {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.ApiGetGain();
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  ");
                try {
                    DashBoardTableParser dashBoardTableParser = new DashBoardTableParser(response.body().string());
                    dashBoardTableParser.parse();
                    tableContentsList = dashBoardTableParser.getTableContentArrayStrings();
                    headers = dashBoardTableParser.getHeadersArrayList();

                    Log.d("sdkjfsdklfjdkf", tableContentsList.size() + "   " + headers.size());
                    dashBoardView.setGainAdapter(headers, tableContentsList);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getLoss() {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.ApiGetLoss();
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  ");
                try {
                    DashBoardTableParser dashBoardTableParser = new DashBoardTableParser(response.body().string());
                    dashBoardTableParser.parse();
                    tableContentsList = dashBoardTableParser.getTableContentArrayStrings();
                    headers = dashBoardTableParser.getHeadersArrayList();

                    Log.d("sdkjfsdklfjdkf", tableContentsList.size() + "   " + headers.size());
                    dashBoardView.setLossAdapter(headers, tableContentsList);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }





    @Override
    public void getGraphData(String selectedIndex, int month) {

        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getDashboardChartData(selectedIndex, month);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("getGraphData", response.code() + "  ");

                if(response.isSuccessful()) {
                    try {
                        DashboardChartParser dashboardChartParser = new DashboardChartParser(response.body().string());
                        dashboardChartParser.parser();
                        if(dashboardChartParser.chartData.size() == 0){
//                            dashBoardView.nodDataFound();
                        }else {
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }


}
