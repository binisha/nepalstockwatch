package com.zeronebits.nepalstockwatch.dashboard.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Own on 9/1/2017.
 */

public class DashBoardTableParser {

    String result;
    ArrayList<ArrayList<String>> TableContentArrayStrings;
    ArrayList<String> headersArrayList;

    public ArrayList<ArrayList<String>> gainTableContentArrayStrings;
    public ArrayList<String> gainHeadersArrayList;

    public ArrayList<ArrayList<String>> turnOverTableContentArrayStrings;
    public ArrayList<String>turnOVerHeadersArrayList;

    public ArrayList<ArrayList<String>> shareTableContentArrayStrings;
    public ArrayList<String>shareHeadersArrayList;

    public ArrayList<ArrayList<String>> lossTableContentArrayStrings;
    public ArrayList<String> lossHeadersArrayList;

    public MarketSummaryModel marketSummaryModel;

    public ArrayList<ArrayList<String>> getTableContentArrayStrings() {
        return TableContentArrayStrings;
    }

    public ArrayList<String> getHeadersArrayList() {
        return headersArrayList;
    }

    public DashBoardTableParser(String result) {
        this.result = result;
        TableContentArrayStrings = new ArrayList<>();
        headersArrayList = new ArrayList<>();

        gainTableContentArrayStrings = new ArrayList<>();
        gainHeadersArrayList = new ArrayList<>();

        turnOVerHeadersArrayList = new ArrayList<>();
        turnOverTableContentArrayStrings = new ArrayList<>();

        shareTableContentArrayStrings = new ArrayList<>();
        shareHeadersArrayList = new ArrayList<>();

        lossHeadersArrayList = new ArrayList<>();
        lossTableContentArrayStrings = new ArrayList<>();
    }

 /*   public void parse() throws JSONException {
        DashBoardTableModel dashBoardTableModel = new DashBoardTableModel();
        JSONObject jsonObject = new JSONObject(result);
        JSONArray jsonArray = jsonObject.getJSONArray("Head");
        for(int i=0; i < jsonArray.length(); i++){
            Log.d("checkingJsonArray",jsonArray.getString(i));
            headersArrayList.add(jsonArray.getString(i));
        }
        dashBoardTableModel.setContents(headersArrayList);
        JSONArray dataArray = jsonObject.getJSONArray("data");
        for(int i = 0; i < dataArray.length(); i++){
            JSONArray stringArray = dataArray.getJSONArray(i);
            ArrayList<String> dataStrings = new ArrayList<>();
            for(int a = 0; a < stringArray.length(); a ++){
                dataStrings.add(stringArray.getString(a));
            }
            TableContentArrayStrings.add(dataStrings);
        }
    }*/

    public void parse() throws JSONException {
        DashBoardTableModel dashBoardTableModel = new DashBoardTableModel();
        JSONObject jsonObject = new JSONObject(result);
        JSONArray headerJson = jsonObject.getJSONArray(jsonObject.names().getString(0));
        for (int h = 0; h < headerJson.length(); h++) {
            headersArrayList.add(headerJson.getString(h));
            Log.d("asdsd", headerJson.getString(h));
        }
        for (int i = 1; i < jsonObject.length(); i++) {
            JSONArray jsonArray = jsonObject.getJSONArray(jsonObject.names().getString(i));
            ArrayList<String> dataStrings = new ArrayList<>();
            for (int a = 0; a < jsonArray.length(); a++) {
                dataStrings.add(jsonArray.getString(a));
            }
            TableContentArrayStrings.add(dataStrings);
        }
    }


    public void parser() throws JSONException {
        JSONObject jsonObject = new JSONObject(result);
        Log.d("dnfdfmdsf", result);
        JSONObject marketSummary = jsonObject.getJSONObject("marketSummary");
        marketSummaryModel = new MarketSummaryModel();
        marketSummaryModel.setCurrentIndex(marketSummary.getString( "Current Index"));
        marketSummaryModel.setFloatedMarketCapitalizationRs(marketSummary.getString("Total Market Capitalization Rs."));
        marketSummaryModel.setPercentChange(marketSummary.getString("Percent Change"));
        marketSummaryModel.setPointChange(marketSummary.getString("Point Change"));
        marketSummaryModel.setTotalMarketCapitalizationRs(marketSummary.getString("Floated Market Capitalization Rs."));
        marketSummaryModel.setTotalScripsTraded(marketSummary.getString("Total Scrips Traded"));
        marketSummaryModel.setTotalTradedShares(marketSummary.getString("Total Traded Shares"));
        marketSummaryModel.setTotalTransactions(marketSummary.getString("Total Transactions"));

        /*Gain top 10*/
        JSONObject gain = jsonObject.getJSONObject("top10ByGain");
        JSONArray gainHeader = gain.getJSONArray("Head");
        for (int h = 0; h < gainHeader.length(); h++) {
            gainHeadersArrayList.add(gainHeader.getString(h));
        }

        Iterator<String> gainKeys= gain.keys();
        while (gainKeys.hasNext())
        {
            String keyValue = (String)gainKeys.next();
            JSONArray jsonArray = gain.getJSONArray(keyValue);
            ArrayList<String> dataStrings = new ArrayList<>();
            for (int a = 0; a < jsonArray.length(); a++) {
                Log.d("dsfdsfdf",jsonArray.getString(a));
                dataStrings.add(jsonArray.getString(a));
            }
            gainTableContentArrayStrings.add(dataStrings);
        }
       /* for (int i = 0; i < gain.length(); i++) {
            JSONArray jsonArray = gain.getJSONArray(jsonObject.names().getString(i));
            ArrayList<String> dataStrings = new ArrayList<>();

        }*/

    /*turnOver top 10*/
        JSONObject turnOver = jsonObject.getJSONObject("top10ByTurnOver");
        JSONArray turnOverHeader = turnOver.getJSONArray("Head");
        for (int h = 0; h < turnOverHeader.length(); h++) {
            turnOVerHeadersArrayList.add(turnOverHeader.getString(h));
        }

        Iterator<String> turnKeys= turnOver.keys();
        while (turnKeys.hasNext()) {
            String keyValue = (String) turnKeys.next();
            JSONArray jsonArray = turnOver.getJSONArray(keyValue);
            ArrayList<String> dataStrings = new ArrayList<>();
            for (int a = 0; a < jsonArray.length(); a++) {
                dataStrings.add(jsonArray.getString(a));
            }
            turnOverTableContentArrayStrings.add(dataStrings);
        }

        /*for (int i = 0; i < turnOver.length(); i++) {
            JSONArray jsonArray = turnOver.getJSONArray(jsonObject.names().getString(i));
            ArrayList<String> dataStrings = new ArrayList<>();

        }*/

        /*share top 10*/
        JSONObject share = jsonObject.getJSONObject("top10ByShareVolume");
        JSONArray shareHeader = share.getJSONArray("Head");
        for (int h = 0; h < shareHeader.length(); h++) {
            shareHeadersArrayList.add(shareHeader.getString(h));
        }

        Iterator<String> shareKeys= share.keys();
        while (shareKeys.hasNext()) {
            String keyValue = (String) shareKeys.next();
            JSONArray jsonArray = turnOver.getJSONArray(keyValue);
            ArrayList<String> dataStrings = new ArrayList<>();
            for (int a = 0; a < jsonArray.length(); a++) {
                dataStrings.add(jsonArray.getString(a));
            }
            shareTableContentArrayStrings.add(dataStrings);
        }
        /*for (int i = 0; i < share.length(); i++) {
            JSONArray jsonArray = share.getJSONArray(jsonObject.names().getString(i));
            ArrayList<String> dataStrings = new ArrayList<>();

        }*/


        /*loss top 10*/
        JSONObject loss = jsonObject.getJSONObject("top10ByGain");
        JSONArray lossHeader = loss.getJSONArray("Head");
        for (int h = 0; h < lossHeader.length(); h++) {
            lossHeadersArrayList.add(lossHeader.getString(h));
        }
        Iterator<String> lossKeys= loss.keys();
        while (lossKeys.hasNext()) {
            String keyValue = (String) lossKeys.next();
            JSONArray jsonArray = loss.getJSONArray(keyValue);
            ArrayList<String> dataStrings = new ArrayList<>();
            for (int a = 0; a < jsonArray.length(); a++) {
                dataStrings.add(jsonArray.getString(a));
            }
            lossTableContentArrayStrings.add(dataStrings);
      /*  for (int i = 0; i < loss.length(); i++) {
            JSONArray jsonArray = loss.getJSONArray(jsonObject.names().getString(i));
            ArrayList<String> dataStrings = new ArrayList<>();
           */
        }
    }

}
