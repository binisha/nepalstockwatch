package com.zeronebits.nepalstockwatch.dashboard.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Own on 11/15/2017.
 */

public class MarketSummaryModel {

    @SerializedName("Total Transactions")
    @Expose
    private String totalTransactions;
    @SerializedName("Point Change")
    @Expose
    private String pointChange;
    @SerializedName("Current Index")
    @Expose
    private String currentIndex;
    @SerializedName("Total Scrips Traded")
    @Expose
    private String totalScripsTraded;
    @SerializedName("Total Traded Shares")
    @Expose
    private String totalTradedShares;
    @SerializedName("Floated Market Capitalization Rs.")
    @Expose
    private String floatedMarketCapitalizationRs;
    @SerializedName("Percent Change")
    @Expose
    private String percentChange;
    @SerializedName("Total Market Capitalization Rs.")
    @Expose
    private String totalMarketCapitalizationRs;

    public String getTotalTransactions() {
        return totalTransactions;
    }

    public void setTotalTransactions(String totalTransactions) {
        this.totalTransactions = totalTransactions;
    }

    public String getPointChange() {
        return pointChange;
    }

    public void setPointChange(String pointChange) {
        this.pointChange = pointChange;
    }

    public String getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(String currentIndex) {
        this.currentIndex = currentIndex;
    }

    public String getTotalScripsTraded() {
        return totalScripsTraded;
    }

    public void setTotalScripsTraded(String totalScripsTraded) {
        this.totalScripsTraded = totalScripsTraded;
    }

    public String getTotalTradedShares() {
        return totalTradedShares;
    }

    public void setTotalTradedShares(String totalTradedShares) {
        this.totalTradedShares = totalTradedShares;
    }

    public String getFloatedMarketCapitalizationRs() {
        return floatedMarketCapitalizationRs;
    }

    public void setFloatedMarketCapitalizationRs(String floatedMarketCapitalizationRs) {
        this.floatedMarketCapitalizationRs = floatedMarketCapitalizationRs;
    }

    public String getPercentChange() {
        return percentChange;
    }

    public void setPercentChange(String percentChange) {
        this.percentChange = percentChange;
    }

    public String getTotalMarketCapitalizationRs() {
        return totalMarketCapitalizationRs;
    }

    public void setTotalMarketCapitalizationRs(String totalMarketCapitalizationRs) {
        this.totalMarketCapitalizationRs = totalMarketCapitalizationRs;
    }
}
