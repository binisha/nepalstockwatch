package com.zeronebits.nepalstockwatch.dashboard.presentar;

/**
 * Created by Own on 9/1/2017.
 */

public interface DashBoardTablePresenter {

    void getAllDashboardData();
    void getTurnedOver();
    void getShareTraded();
    void getNoOfTransaction();
    void getGain();
    void getLoss();

    void getGraphData(String selectedIndex, int month);
}
