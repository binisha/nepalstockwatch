package com.zeronebits.nepalstockwatch.notification;

import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.zeronebits.nepalstockwatch.alerts.AlertsMessagingActivity;
import com.zeronebits.nepalstockwatch.alerts.model.MessagingModel;
import com.zeronebits.nepalstockwatch.alerts.view.AlertsFragment;
import com.zeronebits.nepalstockwatch.mainapplication.ApplicationMain;
import com.zeronebits.nepalstockwatch.realmdatabase.RealmController;
import com.zeronebits.nepalstockwatch.splash.view.SplashActivity;
import com.zeronebits.nepalwatchstock.R;


/**
 * Created by Ashraf on 3/5/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG,remoteMessage.getData().get("_key"));

        RealmController realmController ;
        try{
            realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
            MessagingModel messagingModel = new MessagingModel();
            messagingModel.setId(remoteMessage.getData().get("_key"));
            messagingModel.setTitle(remoteMessage.getData().get("title"));
            messagingModel.setMessage(remoteMessage.getData().get("message"));
            realmController.addMessage(messagingModel);
        }catch (Exception e){
            RealmController.instance = null;
            realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
            MessagingModel messagingModel = new MessagingModel();
            messagingModel.setId(remoteMessage.getData().get("_key"));
            messagingModel.setTitle(remoteMessage.getData().get("title"));
            messagingModel.setMessage(remoteMessage.getData().get("message"));
            realmController.addMessage(messagingModel);
            e.printStackTrace();
        }

        sendNotification(remoteMessage.getData().get("title"),remoteMessage.getData().get("message"), Integer.parseInt(remoteMessage.getData().get("_key")));
    }


    private void sendNotification(String title,String message,int id) {
        Intent intent = new Intent(this, AlertsMessagingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, id, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.nepse_app_icon)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(id, notificationBuilder.build());
    }
}
