package com.zeronebits.nepalstockwatch.companyname.model;

import java.util.ArrayList;

import io.realm.RealmObject;

/**
 * Created by Own on 11/14/2017.
 */

public class IndexModel extends RealmObject {

    String indexs;

    public String getIndexs() {
        return indexs;
    }

    public void setIndexs(String indexs) {
        this.indexs = indexs;
    }
}
