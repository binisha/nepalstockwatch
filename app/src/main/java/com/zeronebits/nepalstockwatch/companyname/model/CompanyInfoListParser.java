package com.zeronebits.nepalstockwatch.companyname.model;

import android.content.Context;
import android.util.Log;

import com.zeronebits.nepalstockwatch.mainapplication.ApplicationMain;
import com.zeronebits.nepalstockwatch.realmdatabase.RealmController;
import com.zeronebits.nepalstockwatch.utils.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Own on 11/5/2017.
 */

public class CompanyInfoListParser {

    String result;
    Context context;


    public CompanyInfoListParser(String result,Context context) {
        this.result = result;
        this.context = context;
    }

    public void parser() throws JSONException {
        Log.d("dfnsdjfdsf",result);
        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        realmController.clearAllCompanyNames();
        realmController.clearAllIndex();
        realmController.clearAllSubIndex();

        JSONObject data = new JSONObject(result);
        JSONArray indices = data.getJSONArray("indices");
        JSONArray subIndices = data.getJSONArray("sub-indices");
        JSONArray company = data.getJSONArray("company-list");

        Constant.companyList = new ArrayList<>();
        for(int i=0; i < company.length(); i++){
            JSONObject jsonObject = company.getJSONObject(i);
            CompanyNameModel companyNameMode = new CompanyNameModel();
            companyNameMode.setId(jsonObject.getString("id"));
            companyNameMode.setCode(jsonObject.getString("symbol"));
            companyNameMode.setName(jsonObject.getString("name"));
            Constant.companyList.add(jsonObject.getString("name"));
            realmController.addCompanyName(companyNameMode);
        }
        Constant.indexList = new ArrayList<>();
        for(int i=0; i < indices.length(); i++){
            IndexModel indexModel = new IndexModel();
            indexModel.setIndexs(indices.getString(i));
            Constant.indexList.add(indices.getString(i));
            realmController.addIndex(indexModel);
        }

        Constant.subIndexList = new ArrayList<>();
        for(int i=0; i < subIndices.length(); i++){
            SubIndexModel subIndexModel = new SubIndexModel();
            subIndexModel.setIndexs(subIndices.getString(i));
            Constant.subIndexList.add(subIndices.getString(i));
            realmController.addSubIndex(subIndexModel);
        }



    }
}
