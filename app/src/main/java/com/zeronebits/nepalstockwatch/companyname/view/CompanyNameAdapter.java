package com.zeronebits.nepalstockwatch.companyname.view;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.companyname.model.CompanyNameModel;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.view.CompanyInfoView;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Own on 11/5/2017.
 */

public class CompanyNameAdapter extends ArrayAdapter<CompanyNameModel> {
    private final Context mContext;
    private final List<CompanyNameModel> companyNameModels;
    private final List<CompanyNameModel> companyNameModels_all;
    private final List<CompanyNameModel> companyNameModels_suggestion;
    private final int mLayoutResourceId;
    CompanyNameView companyNameView;

    public CompanyNameAdapter(Context context, int resource, List<CompanyNameModel> companyNameModels) {
        super(context, resource, companyNameModels);
        this.mContext = context;
        this.mLayoutResourceId = resource;
        this.companyNameModels = new ArrayList<>(companyNameModels);
        this.companyNameModels_all = new ArrayList<>(companyNameModels);
        this.companyNameModels_suggestion = new ArrayList<>();
        this.companyNameView = companyNameView;
    }

    public int getCount() {
        return companyNameModels.size();
    }

    public CompanyNameModel getItem(int position) {
        return companyNameModels.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(mLayoutResourceId, parent, false);
            }
            final CompanyNameModel companyNameModel = getItem(position);
            final TextView name = (TextView) convertView.findViewById(R.id.tv_company_name);
            name.setText(companyNameModel.getName());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            public String convertResultToString(Object resultValue) {
                return ((CompanyNameModel) resultValue).getCode();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (constraint != null) {
                    companyNameModels_suggestion.clear();
                    for (CompanyNameModel companyNameModel : companyNameModels_all) {
                        if (companyNameModel.getCode().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                            companyNameModels_suggestion.add(companyNameModel);
                        }
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = companyNameModels_suggestion;
                    filterResults.count = companyNameModels_suggestion.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                companyNameModels.clear();
                if (results != null && results.count > 0) {
                    // avoids unchecked cast warning when using companyNameModels.addAll((ArrayList<Department>) results.values);
                    List<?> result = (List<?>) results.values;
                    for (Object object : result) {
                        if (object instanceof CompanyNameModel) {
                            companyNameModels.add((CompanyNameModel) object);
                        }
                    }
                } else if (constraint == null) {
                    // no filter, add entire original list back in
                    companyNameModels.addAll(companyNameModels_all);
                }
                notifyDataSetChanged();
            }
        };
    }
}