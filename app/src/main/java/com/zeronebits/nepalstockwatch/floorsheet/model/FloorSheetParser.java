package com.zeronebits.nepalstockwatch.floorsheet.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Own on 9/12/2017.
 */

public class FloorSheetParser {

    String result;
    ArrayList<String> headers;
    ArrayList<String> contents;
    ArrayList<ArrayList<String>> contentsWithValue;
    public static int  page = 1;
    public static int last_page = 10;
    public static String page_url = "";
    public String date;
    public ArrayList<String> getHeaders() {
        return headers;
    }

    public ArrayList<String> getContents() {
        return contents;
    }

    public ArrayList<ArrayList<String>> getContentsWithValue() {
        return contentsWithValue;
    }

    public FloorSheetParser(String result) {
        this.result = result;
        headers = new ArrayList<>();
        contentsWithValue = new ArrayList<>();
    }

    public void parse() throws JSONException {
        page += 1;
        JSONObject jsonObject = new JSONObject(result);
        Log.d("dfhsdfdsf",result);
        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
        last_page = jsonObject1.getInt("last_page");
        page_url = jsonObject1.getString("next_page_url");
        JSONArray data = jsonObject1.getJSONArray("data");
        JSONArray headerJson = jsonObject.getJSONArray("header");
        for(int h=0; h< headerJson.length(); h++){
            if(h ==1) {
            }else{
                headers.add(headerJson.getString(h));
            }
        }

        for (int i = 0; i < data.length(); i++) {
            JSONObject jsonArray = data.getJSONObject(i);
            contents = new ArrayList<>();
            for(int a = 0; a < jsonArray.length(); a++ ){
                if(a == 1){
                    date = jsonArray.getString(jsonArray.names().getString(a));
                }else {
                    contents.add(jsonArray.getString(jsonArray.names().getString(a)));
                }
            }
            contentsWithValue.add(contents);
        }
    }

}
