package com.zeronebits.nepalstockwatch.floorsheet.presentar;

/**
 * Created by Own on 9/2/2017.
 */

public interface FloorSheetPresenter {

    void getFloorSheetData(int page);

    void loadNextPage(String page);

    void getFloorSheetDataofCompany(int page, String companyCode);


}
