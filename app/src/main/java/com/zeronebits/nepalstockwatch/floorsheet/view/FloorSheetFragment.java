package com.zeronebits.nepalstockwatch.floorsheet.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.inqbarna.tablefixheaders.Recycler;
import com.inqbarna.tablefixheaders.TableFixHeaders;
import com.wang.avi.AVLoadingIndicatorView;
import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyname.model.CompanyNameModel;
import com.zeronebits.nepalstockwatch.companyname.view.CompanyNameAdapter;
import com.zeronebits.nepalstockwatch.companyname.view.CompanyNameView;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.model.CompanyInfoModel;
import com.zeronebits.nepalstockwatch.companyprofile.view.CompanyProfile;
import com.zeronebits.nepalstockwatch.floorsheet.model.FloorSheetParser;
import com.zeronebits.nepalstockwatch.floorsheet.presentar.FloorSheetImplementor;
import com.zeronebits.nepalstockwatch.floorsheet.presentar.FloorSheetPresenter;
import com.zeronebits.nepalstockwatch.livetrading.view.LiveTradingAdapter;
import com.zeronebits.nepalstockwatch.livetrading.view.LiveTradingFragment;
import com.zeronebits.nepalstockwatch.mainactivitynotlogin.view.MainActivityNotLogin;
import com.zeronebits.nepalstockwatch.mainapplication.ApplicationMain;
import com.zeronebits.nepalstockwatch.realmdatabase.RealmController;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.PaginationScrollListener;
import com.zeronebits.nepalstockwatch.watchlist.model.WatchListModelDatabase;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import rx.internal.operators.CompletableOnSubscribeConcat;

/**
 * Created by Own on 8/26/2017.
 */

public class FloorSheetFragment extends BaseFragment implements FloorSheetView {

    /*  @BindView(R.id.table)
      TableFixHeaders tableFixHeaders;*/
    @BindView(R.id.avi)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.et_floor_sheet_buyer)
    EditText buyerEt;
    @BindView(R.id.et_floor_sheet_seller)
    EditText sellerEt;
    @BindView(R.id.act_company_name)
    AutoCompleteTextView companySymbol;
    FloorSheetAdapter baseTableAdapter;
    CompanyNameAdapter companyNameAdapter;
    ArrayList<CompanyNameModel> companyNameModels;
    MainActivity mainActivity;
    MainActivityNotLogin mainActivityNotLogin;
    @BindView(R.id.rv_floor_sheet)
    RecyclerView recyclerView;
    @BindView(R.id.tv_profile_code)
    TextView profile;
    @BindView(R.id.tv_buyer)
    TextView buyer;
    @BindView(R.id.tv_seller)
    TextView seller;
    @BindView(R.id.tv_units)
    TextView units;
    @BindView(R.id.tv_rate)
    TextView rate;
    @BindView(R.id.ll_livetrading_error)
    LinearLayout errorLayout;
    FloorSheetPresenter floorSheetPresenter;
    FloorSheetAdapterRecycler floorSheetAdapterRecycler;
    ArrayList<ArrayList<String>> allContents;

    @BindView(R.id.sw_live_trading_troggle)
    Switch watchListOrAll;
    private static final int PAGE_START = 1;

    ArrayList<String> headers;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to modify.
    private int currentPage = PAGE_START;
    LinearLayoutManager linearLayoutManager;
    @BindView(R.id.tv_error_msg)
    TextView errorMsg;
    @Override
    protected int getLayout() {
        return R.layout.floor_sheet;
    }

    @Override
    protected void init(View view) {
        allContents = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        loadingIndicatorView.setVisibility(View.VISIBLE);
//        tableFixHeaders.setVisibility(View.GONE);
        floorSheetPresenter = new FloorSheetImplementor(this,getActivity());
        floorSheetPresenter.getFloorSheetData(FloorSheetParser.page);
        companySymbol.setThreshold(0);

        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        companyNameModels = realmController.getModelList();

        companyNameAdapter = new CompanyNameAdapter(getActivity(), R.layout.simple_text_view, companyNameModels);
        companySymbol.setAdapter(companyNameAdapter);

            watchListOrAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (Constant.ACESS_TOKEN.equalsIgnoreCase("")) {
                        Toast.makeText(getActivity(), "Login Required", Toast.LENGTH_SHORT).show();
                        buttonView.setChecked(false);
                    }else {
                        if (!isChecked) {
                            floorSheetAdapterRecycler = new FloorSheetAdapterRecycler(allContents, getActivity(), FloorSheetFragment.this);
                            recyclerView.setAdapter(floorSheetAdapterRecycler);
                            if (currentPage != FloorSheetParser.last_page)
                                floorSheetAdapterRecycler.addLoadingFooter();
                        } else {
                            if (getWatchListContents().size() < 0) {
                                loadingIndicatorView.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                                errorLayout.setVisibility(View.VISIBLE);
                                errorMsg.setText("watchlist not added");
                            } else {
                                floorSheetAdapterRecycler.removeLoadingFooter();
                                floorSheetAdapterRecycler = new FloorSheetAdapterRecycler(getWatchListContents(), getActivity(), FloorSheetFragment.this);
                                recyclerView.setAdapter(floorSheetAdapterRecycler);
                            }
                        }
                    }
                }
            });
    }

    private ArrayList<ArrayList<String>> getWatchListContents() {
        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        ArrayList<WatchListModelDatabase> watchListUserModels = realmController.getWatchList();
        ArrayList<ArrayList<String>> watchList = new ArrayList<>();
        for(WatchListModelDatabase watchListModelDatabase: watchListUserModels){
            Log.d("dfdsfsdfsdf",watchListModelDatabase.getCompanySymbol());
            for (final ArrayList<String> contents : allContents) {
                if (contents.contains(watchListModelDatabase.getCompanySymbol())) {
                    watchList.add(contents);
                }else{
                    Log.d("dfdsfsdfsdf","hjrtr");
                }
            }
        }


        return watchList;
    }

    @Override
    public void setAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> contents,String date) {
        try {
            mainActivity.setToolBarTitle("Floorsheet (" + date + ")");
        }catch (Exception e){
            mainActivityNotLogin.setToolBarTitle("Floorsheet (" + date + ")",true);
        }
        allContents.addAll(contents);
        loadingIndicatorView.setVisibility(View.GONE);
        profile.setText(headers.get(0));
        buyer.setText(headers.get(1));
        seller.setText(headers.get(2));
        units.setText(headers.get(3));
        rate.setText(headers.get(4));
        floorSheetAdapterRecycler = new FloorSheetAdapterRecycler(contents, getActivity(), this);
        recyclerView.setAdapter(floorSheetAdapterRecycler);
        if (currentPage != FloorSheetParser.last_page) floorSheetAdapterRecycler.addLoadingFooter();

        recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                if (floorSheetAdapterRecycler.isLoadingAdded) {
                    floorSheetPresenter.loadNextPage("public" + FloorSheetParser.page_url);
                }
            }

            @Override
            public int getTotalPageCount() {
                return FloorSheetParser.last_page;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        buyerEt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                filterBuyer(cs.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });


        sellerEt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                filterSeller(cs.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        companySymbol.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                filterCompany(cs.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
      /*  sellerEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    buyerEt.setText("");
                }
            }
        });
        buyerEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    sellerEt.setText("");
                }
            }
        });
*/
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mainActivity = (MainActivity) getActivity();
            mainActivity.setToolBarTitle("Floorsheet");
        } catch (Exception e) {
            mainActivityNotLogin = (MainActivityNotLogin) getActivity();
            mainActivityNotLogin.setToolBarTitle("Floorsheet",true);
        }
    }

    @Override
    public void symbolClicked(String s) {
        Fragment fragment = new CompanyProfile();
        Bundle b = new Bundle();
        b.putString(Constant.COMPANYCODE, s);
        fragment.setArguments(b);
        if (Constant.ACESS_TOKEN.equalsIgnoreCase("")) {
            getFragmentManager().beginTransaction().replace(R.id.fl_activity_main_not_login, fragment).addToBackStack("tag").commit();
        } else {
            getFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame, fragment).addToBackStack("tag").commit();
        }
    }


    @Override
    public void setNewAdapter(ArrayList<ArrayList<String>> tableContentsList) {
        allContents.addAll(tableContentsList);
        floorSheetAdapterRecycler.removeLoadingFooter();
        isLoading = false;
        Log.d("dsfhsdfdsf",tableContentsList.size() + " ");
        floorSheetAdapterRecycler.addAll(tableContentsList);

        if (currentPage != FloorSheetParser.last_page) floorSheetAdapterRecycler.addLoadingFooter();
        else isLastPage = true;
    }

    @Override
    public void errorLayout() {
        errorLayout.setVisibility(View.VISIBLE);
        loadingIndicatorView.setVisibility(View.GONE);

    }


    void filterCompany(String text) {
        if (floorSheetAdapterRecycler.isLoadingAdded) {
            floorSheetAdapterRecycler.isLoadingAdded = false;
            floorSheetAdapterRecycler.removeLoadingFooter();
        }

        if (text.length() == 0) {
            if (!floorSheetAdapterRecycler.isLoadingAdded) {
                floorSheetAdapterRecycler.addLoadingFooter();
            }
        }
        ArrayList<ArrayList<String>> temp = new ArrayList();
        for (ArrayList<String> d : allContents) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            Log.d("CheckingCrashArray", d.size()+"");
            Log.d("CheckingCrashArray2", text+"");
            if (d.get(0).toLowerCase().trim().contains(text)) {
                temp.add(d);
            }
        }
        //update recyclerview
        floorSheetAdapterRecycler.updateList(temp);
    }

    void filterBuyer(String text) {
        if (floorSheetAdapterRecycler.isLoadingAdded) {
            floorSheetAdapterRecycler.isLoadingAdded = false;
            floorSheetAdapterRecycler.removeLoadingFooter();
        }

        if (text.length() == 0) {
            if (!floorSheetAdapterRecycler.isLoadingAdded) {
                floorSheetAdapterRecycler.addLoadingFooter();
            }
        }
        ArrayList<ArrayList<String>> temp = new ArrayList();
        for (ArrayList<String> d : allContents) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.get(1).toLowerCase().trim().contains(text)) {
                temp.add(d);
            }
        }
        //update recyclerview
        floorSheetAdapterRecycler.updateList(temp);
    }

    void filterSeller(String text) {
        if (floorSheetAdapterRecycler.isLoadingAdded) {
            floorSheetAdapterRecycler.isLoadingAdded = false;
            floorSheetAdapterRecycler.removeLoadingFooter();
        }

        if (text.length() == 0) {
            if (!floorSheetAdapterRecycler.isLoadingAdded) {
                floorSheetAdapterRecycler.addLoadingFooter();
            }
        }
        ArrayList<ArrayList<String>> temp = new ArrayList();
        for (ArrayList<String> d : allContents) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.get(2).toLowerCase().trim().contains(text)) {
                temp.add(d);
            }
        }
        //update recyclerview
        floorSheetAdapterRecycler.updateList(temp);
    }
}


