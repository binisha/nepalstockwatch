package com.zeronebits.nepalstockwatch.floorsheet.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.floorsheet.presentar.FloorSheetPresenter;
import com.zeronebits.nepalwatchstock.R;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Own on 11/26/2017.
 */

public class FloorSheetAdapterRecycler extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<ArrayList<String>> contents;
    Context context;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private FloorSheetView floorSheetViews;
    private boolean isLoading;
    int editTextPosition;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    public boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;

//    private PaginationAdapterCallback mCallback;

    public int getEditTextPosition() {
        return editTextPosition;
    }

    public void setEditTextPosition(int editTextPosition) {
        this.editTextPosition = editTextPosition;
    }
    public FloorSheetAdapterRecycler(ArrayList<ArrayList<String>> contents, Context context,FloorSheetView floorSheetView) {
        this.context = context;
        this.contents = contents;
        this.floorSheetViews = floorSheetView;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.floor_sheet_single_item, parent, false);
            return new MyViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
      /*  View view = LayoutInflater.from(context).inflate(R.layout.floor_sheet_single_item, parent, false);
        return new MyViewHolder(view);*/
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        if(viewHolder instanceof MyViewHolder) {
            MyViewHolder holder = (MyViewHolder) viewHolder;

            ArrayList<String> content = contents.get(position);
            holder.mainLayout.setBackgroundColor(position % 2 == 0 ? ContextCompat.getColor(context,
                    R.color.bg_table_color1) : ContextCompat.getColor(context, R.color.bg_table_color2));
//            holder.profile.setText(content.get(0));

            SpannableString value = new SpannableString(content.get(0));
            value.setSpan(new UnderlineSpan(), 0, value.length(), 0);
            holder.profile.setText(value);

            holder.buyer.setText(content.get(1));
            holder.seller.setText(content.get(2));
            holder.units.setText(content.get(3));
            holder.rate.setText(content.get(4));

            holder.profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrayList<String> content = contents.get(position);

                    floorSheetViews.symbolClicked(content.get(0));

                }
            });
        }else if (viewHolder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewHolder;
            if (retryPageLoad) {
                loadingViewHolder.progressBar.setVisibility(View.GONE);

            /*    loadingVH.mErrorTxt.setText(
                        errorMsg != null ?
                                errorMsg :
                                context.getString(R.string.error_msg_unknown));*/

            } else {
//                loadingVH.mErrorLayout.setVisibility(View.GONE);
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
            }
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return VIEW_TYPE_ITEM;
        } else {
            return (position == contents.size() - 1 && isLoadingAdded) ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
        }
    }


    @Override
    public int getItemCount() {
        return contents.size();
    }



    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ArrayList<String>());
    }

    public void add(ArrayList<String> r) {
        contents.add(r);
        notifyItemInserted(contents.size() - 1);
    }

    public void addAll(ArrayList<ArrayList<String>> moveResults) {
        for (ArrayList<String> result : moveResults) {
            add(result);
        }
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = contents.size() - 1;
        ArrayList<String> result = contents.get(position);

        if (result != null) {
            contents.remove(position);
            notifyItemRemoved(position);
        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_profile_code)
        TextView profile;
        @BindView(R.id.tv_buyer)
        TextView buyer;
        @BindView(R.id.tv_seller)
        TextView seller;
        @BindView(R.id.tv_units)
        TextView units;
        @BindView(R.id.tv_rate)
        TextView rate;
        @BindView(R.id.ll_main_layout)
        LinearLayout mainLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    // "Loading item" ViewHolder
    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }


    public void updateList(ArrayList<ArrayList<String>> list){
        contents = list;
        notifyDataSetChanged();
    }
}