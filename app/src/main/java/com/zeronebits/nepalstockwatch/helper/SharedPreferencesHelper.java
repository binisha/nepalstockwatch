package com.zeronebits.nepalstockwatch.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.zeronebits.nepalstockwatch.utils.Constant;


public class SharedPreferencesHelper {

    Context context;
    public static String LOGIN = "login";
    public static SharedPreferencesHelper sharedInstance;
    String TOKEN;
    public static String username,password;

    public static SharedPreferencesHelper getSharedInstance(Context context){

        if(sharedInstance == null){
            sharedInstance = new SharedPreferencesHelper(context);
        }
        return sharedInstance;
    }

    public SharedPreferencesHelper(Context context){
        this.context = context;
    }


    public void saveLoginPreferences(String token){
        SharedPreferences.Editor editor = context.getSharedPreferences(LOGIN, context.MODE_PRIVATE).edit();
        editor.putString(Constant.TOKEN,token);
        editor.commit();
    }

    public void saveUsernamePassword(String username,String password){
        SharedPreferences.Editor editor = context.getSharedPreferences(LOGIN, context.MODE_PRIVATE).edit();
        editor.putString(Constant.USERNAME,username);
        editor.putString(Constant.PASSWORD,password);
        editor.commit();
    }


    public String getLoginPreferecnes(){
        SharedPreferences sharedPreferences = context.getSharedPreferences(LOGIN,context.MODE_PRIVATE);
        TOKEN = sharedPreferences.getString(Constant.TOKEN,"");
        return TOKEN;
    }
    public void clearLoginPreferences(){
        SharedPreferences.Editor editor = context.getSharedPreferences(LOGIN, context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
        Constant.ACESS_TOKEN = "";

    }

    public void getUsernameAndPassword(){
        SharedPreferences sharedPreferences = context.getSharedPreferences(LOGIN,context.MODE_PRIVATE);
        username = sharedPreferences.getString(Constant.USERNAME,"");
        password = sharedPreferences.getString(Constant.PASSWORD,"");
    }



}
