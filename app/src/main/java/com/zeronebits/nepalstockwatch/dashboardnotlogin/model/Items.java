package com.zeronebits.nepalstockwatch.dashboardnotlogin.model;

/**
 * Created by Own on 8/29/2017.
 */

public class Items {
    String icon;
    String name;
    int images;

    public Items(String icon, String name,int images) {
        this.icon = icon;
        this.name = name;
        this.images = images;
    }

    public int getImages() {
        return images;
    }

    public String getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }
}
