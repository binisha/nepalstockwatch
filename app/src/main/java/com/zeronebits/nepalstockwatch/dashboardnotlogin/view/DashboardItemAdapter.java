package com.zeronebits.nepalstockwatch.dashboardnotlogin.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.dashboardnotlogin.model.Items;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

/**
 * Created by Own on 8/29/2017.
 */

public class DashboardItemAdapter extends ArrayAdapter<Items> {

    ArrayList<Items> itemses;
    Context context;

    public DashboardItemAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<Items> itemses) {
        super(context, resource, itemses);
        this.context = context;
        this.itemses = itemses;
    }


    @Override
    public int getCount() {
        return itemses.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.main_activity_item_adapter_single, null);
        Items items = itemses.get(position);
        TextView textView = (TextView) view.findViewById(R.id.tt_icon);
        TextView name = (TextView) view.findViewById(R.id.tt_icon_name);
        ImageView itemsIcon = (ImageView) view.findViewById(R.id.iv_items);
        itemsIcon.setImageResource(items.getImages());
//        textView.setText(items.getIcon());
        name.setText(items.getName());
      /*  if(items.getName().equalsIgnoreCase("Profile")){
            Log.d("sdfhdsf","rtr");
            Typeface typeface = Typeface.createFromAsset(context.getAssets(),"fontawesome.ttf");
            textView.setText(items.getIcon());
            textView.setTypeface(typeface);
            name.setText(items.getName());
        }else {
            textView.setText(items.getIcon());
            name.setText(items.getName());
        }*/
        return view;
    }
}
