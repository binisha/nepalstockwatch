package com.zeronebits.nepalstockwatch.verification.presenter;

/**
 * Created by Own on 1/4/2018.
 */

public interface VerificationPresenter {
    void sendVerificationCode(String verificationCode);

    void resendVerificationCode();
}
