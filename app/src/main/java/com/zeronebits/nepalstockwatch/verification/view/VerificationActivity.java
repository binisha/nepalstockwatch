package com.zeronebits.nepalstockwatch.verification.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.base.BaseActivity;
import com.zeronebits.nepalstockwatch.verification.presenter.VerificationImp;
import com.zeronebits.nepalstockwatch.verification.presenter.VerificationPresenter;
import com.zeronebits.nepalwatchstock.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Own on 1/4/2018.
 */

public class VerificationActivity extends BaseActivity implements VerificationView {

    @BindView(R.id.et_verification_code)
    EditText etVerificationCode;
    @BindView(R.id.btn_submit)
    Button submit;
    @BindView(R.id.tv_re_send)
    TextView resend;
    VerificationPresenter verificationPresenter;
    private IntentFilter mIntentFilter;
    String messageBody;
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("dfkshgdsgfdg","here");
            Bundle data = intent.getExtras();
            Object[] pdus = (Object[]) data.get("pdus");
            for (int i = 0; i < pdus.length; i++) {
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
                messageBody = smsMessage.getMessageBody();
            }
            String lastWord = messageBody.substring(messageBody.lastIndexOf(" ") + 1);
            Log.d("dfkshgdsgfdg",lastWord);

            etVerificationCode.setText(lastWord);
        }
    };


    @Override
    protected int getLayout() {
        return R.layout.verification;
    }

    @Override
    protected void init() {
        verificationPresenter = new VerificationImp(this, this);

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        mIntentFilter.setPriority(999);
        registerReceiver(broadcastReceiver, mIntentFilter);
    }

    @OnClick(R.id.btn_submit)
    public void sendVerification() {
        String verificationCode = etVerificationCode.getText().toString();
        if (verificationCode.isEmpty()) {
            etVerificationCode.setError("Required");
        } else {
            verificationPresenter.sendVerificationCode(verificationCode);
        }
    }

    @OnClick(R.id.tv_re_send)
    public void resendCode() {
        etVerificationCode.setText("");
        verificationPresenter.resendVerificationCode();
    }


    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(broadcastReceiver);
        }catch (Exception e){

        }
    }

    @Override
    public void codeResendSucess() {

    }
}
