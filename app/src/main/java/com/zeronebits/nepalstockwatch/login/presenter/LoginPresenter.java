package com.zeronebits.nepalstockwatch.login.presenter;

import com.zeronebits.nepalstockwatch.login.model.LoginUserModel;

/**
 * Created by Own on 10/16/2017.
 */

public interface LoginPresenter {
    void loginUser(LoginUserModel loginUserModel);
}
