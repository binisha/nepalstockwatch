package com.zeronebits.nepalstockwatch.login.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Own on 10/16/2017.
 */

public class LoginUserModel {

    @SerializedName("username")
    String username;
    @SerializedName("password")
    String password;


    public LoginUserModel(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
