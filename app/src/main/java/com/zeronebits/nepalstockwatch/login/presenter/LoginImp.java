package com.zeronebits.nepalstockwatch.login.presenter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.login.model.LoginParser;
import com.zeronebits.nepalstockwatch.login.model.LoginUserModel;
import com.zeronebits.nepalstockwatch.login.view.LoginView;
import com.zeronebits.nepalstockwatch.splash.model.SplashParser;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;
import com.zeronebits.nepalstockwatch.verification.view.VerificationActivity;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 10/16/2017.
 */

public class LoginImp implements LoginPresenter {

    LoginView loginView;
    Context context;
    SharedPreferences sharedPreferencesToken;

    public LoginImp(LoginView loginView, Context context) {
        this.loginView = loginView;
        this.context = context;
    }

    @Override
    public void loginUser(LoginUserModel loginUserModel) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();
        Log.d("CheckingCountryResponse", "i am here");

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.loginUser(loginUserModel);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  ");
                if (response.isSuccessful()) {
                    try {
//                        Log.d("sdkfjdsfsdf", response.body().string());
                        SplashParser loginParser = new SplashParser(response.body().string());
                        loginParser.parser();
                        sendToken(loginParser.token);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                    e.printStackTrace();
                }
                }else if(response.code() == 401){
                    loginView.failedLogin();
                }else if ( response.code() == 409){
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    loginView.sendVerification();
                } else {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                        loginView.failedLogin();
                    }catch (Exception e){

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }


    public void sendToken(final String loginToken) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        sharedPreferencesToken = context.getSharedPreferences("token", context.MODE_PRIVATE);
        String token = sharedPreferencesToken.getString("token","null");
        Log.d("sdjkfsdfsdf",token);
        Call<ResponseBody> getCountry = loginRegInterface.sendToken(token, Constant.USERCODE);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("sendtokenCode", response.code() + "  ");
                if (response.isSuccessful() || response.code() == 402) {
                        loginView.userToken(loginToken);
                }else {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                        loginView.failedLogin();
                    }catch (Exception e){
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

}

