package com.zeronebits.nepalstockwatch.login.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Own on 1/29/2018.
 */

public class UserModelForUpdate {

    @SerializedName("code")
    String code;
    @SerializedName("MRCH_CODE")
    int mrchCode;
    @SerializedName("BROKER_CODE")
    int brokerCode;
    @SerializedName("NAME")
    String name;
    @SerializedName("PHOTO_URL")
    String photoUrl;
    @SerializedName("ADDR")
    String addr;
    @SerializedName("EMAIL")
    String email;
    @SerializedName("DMAT_NO")
    String dmatNo;
    @SerializedName("ALT_PHONE")
    String altPhone;
    @SerializedName("DOB")
    String dob;
    @SerializedName("SQ_CODE")
    int sqCode;
    @SerializedName("SQ_ANS")
    String sqAns;


    public UserModelForUpdate(String code, int mrchCode, int brokerCode, String name, String photoUrl, String addr, String email, String dmatNo, String altPhone, String dob,int sqCode,String sqAns) {
        this.code = code;
        this.mrchCode = mrchCode;
        this.brokerCode = brokerCode;
        this.name = name;
        this.photoUrl = photoUrl;
        this.addr = addr;
        this.email = email;
        this.dmatNo = dmatNo;
        this.altPhone = altPhone;
        this.dob = dob;
        this.sqCode =sqCode;
        this.sqAns = sqAns;
    }
}
