package com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.presenter;

/**
 * Created by Own on 12/26/2017.
 */

public interface CompanyNewsPresenter {
    void getNewsList(String companyCode);
}
