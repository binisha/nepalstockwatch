package com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.view;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.model.CompanyNewsModel;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.presenter.CompanyImp;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.presenter.CompanyNewsPresenter;
import com.zeronebits.nepalstockwatch.companyprofile.view.CompanyProfile;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Own on 12/18/2017.
 */

public class CompanyNew extends BaseFragment implements CompanyNewsView{

    CompanyNewsPresenter companyNewsPresenter;
    @BindView(R.id.rv_company_news)
    RecyclerView companyNews;
    @BindView(R.id.ll_main_layout)
    LinearLayout errorLayout;

    @Override
    protected int getLayout() {
        return R.layout.company_news;
    }

    @Override
    protected void init(View view) {
        companyNewsPresenter = new CompanyImp(this,getActivity());
        companyNewsPresenter.getNewsList(CompanyProfile.companyCode);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        companyNews.setLayoutManager(linearLayoutManager);
        companyNews.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void setCompanyNews(ArrayList<CompanyNewsModel> companyNewsModels) {
        Log.d("dfsdsfdsfdsf", companyNewsModels.size() + "  df");
        if(companyNewsModels.size() == 0){
            errorLayout.setVisibility(View.VISIBLE);
            companyNews.setVisibility(View.GONE);
        }else {
            errorLayout.setVisibility(View.GONE);
            companyNews.setVisibility(View.VISIBLE);
            CompanyNewsApdater companyNewsApdater = new CompanyNewsApdater(companyNewsModels, getActivity());
            companyNews.setAdapter(companyNewsApdater);
        }
    }

    @Override
    public void noDataFound() {
        errorLayout.setVisibility(View.VISIBLE);
        companyNews.setVisibility(View.GONE);
    }
}
