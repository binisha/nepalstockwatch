package com.zeronebits.nepalstockwatch.companyprofile.tab.companydividend.view;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inqbarna.tablefixheaders.adapters.BaseTableAdapter;
import com.zeronebits.nepalstockwatch.portfolio.tabs.PieChartView;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

/**
 * Created by Own on 12/25/2017.
 */

public class CompanyDividendAdapter extends BaseTableAdapter {

    Context context;
    ArrayList<ArrayList<String>> contents;
    int editTextPosition;

    public int getEditTextPosition() {
        return editTextPosition;
    }

    public void setEditTextPosition(int editTextPosition) {
        this.editTextPosition = editTextPosition;
    }

    ArrayList<String> headers;

    private final float density;

    public CompanyDividendAdapter(Context context, ArrayList<String> headers, ArrayList<ArrayList<String>> contents) {
        this.context = context;
        this.headers = headers;
        this.contents = contents;
        density = context.getResources().getDisplayMetrics().density;
    }


    @Override
    public int getRowCount() {
        return contents.size();
    }

    @Override
    public int getColumnCount() {
        return headers.size() - 1;
    }

    @Override
    public View getView(int row, int column, View convertView, ViewGroup parent) {
        final View view;
        switch (getItemViewType(row, column)) {
            case 0:
                view = getFirstHeader(row, column, convertView, parent);
                break;
            case 1:
                view = getHeader(row, column, convertView, parent);
                break;
            case 2:
                view = getFirstBody(row, column, convertView, parent);
                break;
            case 3:
                view = getBody(row, column, convertView, parent);
                break;
          /*  case 4:
                view = getFamilyView(row, column, convertView, parent);
                break;*/
            default:
                throw new RuntimeException("wtf?");
        }
        return view;
    }

    private View getFirstHeader(int row, int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_table_header_first, parent, false);
            ((TextView) convertView.findViewById(android.R.id.text1)).setText(headers.get(0));
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/small_bold.ttf");
            ((TextView) convertView.findViewById(android.R.id.text1)).setTypeface(font);

        }
        return convertView;
    }

    private View getHeader(final int row, final int column, View convertView, ViewGroup parent) {
//        if (convertView == null) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        convertView = inflater.inflate(R.layout.item_table_header, parent, false);

//                convertView =  getActivity().getLayoutInflater().inflate(R.layout.item_table_header, parent, false);
        if (column == headers.size() - 1) {

        } else {
            ((TextView) convertView.findViewById(android.R.id.text1)).setText(headers.get(column + 1));
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/small_bold.ttf");
            ((TextView) convertView.findViewById(android.R.id.text1)).setTypeface(font);

        }
        return convertView;
    }

    private View getFirstBody(final int row, final int column, View convertView, ViewGroup parent) {
//        if (convertView == null) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        convertView = inflater.inflate(R.layout.item_table_first, parent, false);
        TextView contentName = (TextView) convertView.findViewById(android.R.id.text1);
        if (column == headers.size() - 1) {

        } else {
            convertView.setBackgroundResource(row % 2 == 0 ? R.drawable.bg_table_color1 : R.drawable.bg_table_color2);
            String content = getDevice(row).get(column + 1);
            contentName.setText(content);

        }
//        }
        return convertView;
    }

    private View getBody(final int row, final int column, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        convertView = inflater.inflate(R.layout.item_table, parent, false);
        TextView columnName = (TextView) convertView.findViewById(android.R.id.text1);
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/light.ttf");
        ((TextView) convertView.findViewById(android.R.id.text1)).setTypeface(font);
        if (column == headers.size() - 1) {

        } else {
            Log.d("dsfsdfsdfdsf", row + " " + column);
            convertView.setBackgroundResource(row % 2 == 0 ? R.drawable.bg_table_color1 : R.drawable.bg_table_color2);
            columnName.setText(getDevice(row).get(column + 1));
        }
        return convertView;
    }

    @Override
    public int getWidth(int column) {
        return Math.round(80 * density);
    }

    @Override
    public int getHeight(int row) {
        final int height;
        if (row == -1) {
            height = 35;
        } else if (isFamily(row)) {
            height = 35;
        } else {
            height = 35;
        }
        return Math.round(height * density);
    }

    @Override
    public int getItemViewType(int row, int column) {
        final int itemViewType;
        if (row == -1 && column == -1) {
            itemViewType = 0;
        } else if (row == -1) {
            itemViewType = 1;
        } /*else if (isFamily(row)) {
            itemViewType = 4;
        } */ else if (column == -1) {
            itemViewType = 2;
        } else {
            itemViewType = 3;
        }
        return itemViewType;
    }

    private boolean isFamily(int row) {
        return row == 0;
    }


    private ArrayList<String> getDevice(int row) {
        Log.d("sdjhfsdf", row + "");
        return contents.get(row);
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }


}
