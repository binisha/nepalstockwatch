package com.zeronebits.nepalstockwatch.companyprofile.tab.companydividend.view;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inqbarna.tablefixheaders.TableFixHeaders;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companydividend.presenter.CompanyDividendImpl;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companydividend.presenter.CompanyDividendPresenter;
import com.zeronebits.nepalstockwatch.companyprofile.view.CompanyProfile;
import com.zeronebits.nepalstockwatch.portfolio.tabs.CompanyPortfolioTableAdapter;
import com.zeronebits.nepalwatchstock.R;


import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Own on 12/25/2017.
 */

public class CompanyDividend extends BaseFragment implements CompanyDividendView {

    @BindView(R.id.table)
    TableFixHeaders tableFixHeaders;
    CompanyDividendAdapter baseTableAdapter;
    @BindView(R.id.avi)
    LinearLayout loading;
    @Override
    protected int getLayout() {
        return R.layout.portfolio_company_profile;
    }


    @Override
    protected void init(View view) {
        CompanyDividendPresenter companyDividendPresenter = new CompanyDividendImpl(this,getActivity());
        companyDividendPresenter.getDividendData(CompanyProfile.companyCode);

    }

    @Override
    public void companyTableData(ArrayList<String> headers, ArrayList<ArrayList<String>> contentsWithValue) {
        baseTableAdapter = new CompanyDividendAdapter(getActivity(), headers, contentsWithValue);
        tableFixHeaders.setAdapter(baseTableAdapter);
        loading.setVisibility(View.GONE);
    }
}
