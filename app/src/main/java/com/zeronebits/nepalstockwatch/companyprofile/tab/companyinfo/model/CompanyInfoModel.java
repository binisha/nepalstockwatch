package com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.model;

/**
 * Created by Own on 10/24/2017.
 */

public class CompanyInfoModel {

    String prflTypeCOde;
    String indexCode;
    String code;
    String name;
    String decr;
    String addr;
    String email;
    String photoUrl;
    String url;
    String fcVal;
    String ltp;
    String tls;
    String puv;
    String change;
    String reverse;
    String eps;
    String pv;
    String bookValue;
    String diff;
    String diffP;


    public String getDiff() {
        return diff;
    }

    public void setDiff(String diff) {
        this.diff = diff;
    }

    public String getDiffP() {
        return diffP;
    }

    public void setDiffP(String diffP) {
        this.diffP = diffP;
    }

    public String getReverse() {
        return reverse;
    }

    public void setReverse(String reverse) {
        this.reverse = reverse;
    }

    public String getEps() {
        return eps;
    }

    public void setEps(String eps) {
        this.eps = eps;
    }

    public String getPv() {
        return pv;
    }

    public void setPv(String pv) {
        this.pv = pv;
    }

    public String getBookValue() {
        return bookValue;
    }

    public void setBookValue(String bookValue) {
        this.bookValue = bookValue;
    }

    public String getTls() {
        return tls;
    }

    public void setTls(String tls) {
        this.tls = tls;
    }

    public String getPuv() {
        return puv;
    }

    public void setPuv(String puv) {
        this.puv = puv;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public String getPrflTypeCOde() {
        return prflTypeCOde;
    }

    public void setPrflTypeCOde(String prflTypeCOde) {
        this.prflTypeCOde = prflTypeCOde;
    }

    public String getIndexCode() {
        return indexCode;
    }

    public void setIndexCode(String indexCode) {
        this.indexCode = indexCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDecr() {
        return decr;
    }

    public void setDecr(String decr) {
        this.decr = decr;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFcVal() {
        return fcVal;
    }

    public void setFcVal(String fcVal) {
        this.fcVal = fcVal;
    }

    public String getLtp() {
        return ltp;
    }

    public void setLtp(String ltp) {
        this.ltp = ltp;
    }
}
