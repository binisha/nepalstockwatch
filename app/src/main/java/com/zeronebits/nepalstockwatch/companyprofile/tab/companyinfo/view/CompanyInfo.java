package com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.view;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.model.CompanyInfoModel;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.presenter.CompanyInfoImp;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.presenter.CompanyInfoPresenter;
import com.zeronebits.nepalstockwatch.companyprofile.view.CompanyProfile;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalwatchstock.R;

import butterknife.BindView;

/**
 * Created by Own on 9/21/2017.
 */

public class CompanyInfo extends BaseFragment implements CompanyInfoView {

    @BindView(R.id.tv_company_name)
    TextView companyName;
    @BindView(R.id.tv_company_name_)
    TextView companyName_;
    @BindView(R.id.tv_company_address)
    TextView companyAddress;
    @BindView(R.id.tv_company_email_address)
    TextView companyEmailAddress;
    @BindView(R.id.tv_company_website)
    TextView companyWebsite;
    @BindView(R.id.tv_last_traded_price)
    TextView lastTradedPrice;
    @BindView(R.id.tv_total_listed_shared)
    TextView totalListedShared;
    @BindView(R.id.tv_paid_up_value)
    TextView paidUpValue;
    @BindView(R.id.tv_change_)
    TextView changes;
    @BindView(R.id.tv_reverse)
    TextView reverse;
    @BindView(R.id.tv_eps)
    TextView eps;
    @BindView(R.id.tv_pe)
    TextView pe;
    @BindView(R.id.tv_book_value)
    TextView bookValue;

    @BindView(R.id.ll_main_layout_info)
    LinearLayout mainLayout;
    @BindView(R.id.avi)
    LinearLayout loading;
    @BindView(R.id.tv_no_data_found)
    TextView noDatafound;

    String companyCode;

    @Override
    protected int getLayout() {
        return R.layout.company_info;
    }

    @Override
    protected void init(View view) {
        CompanyInfoPresenter companyInfoPresenter = new CompanyInfoImp(this, getActivity());
        companyInfoPresenter.getCompanyInfo(CompanyProfile.companyCode);
        loading.setVisibility(View.VISIBLE);
    }


    @Override
    public void setCompnayInfo(CompanyInfoModel companyInfoModel) {
        loading.setVisibility(View.GONE);

        mainLayout.setVisibility(View.VISIBLE);
        noDatafound.setVisibility(View.GONE);
        companyName.setText(companyInfoModel.getName());
        companyName_.setText(companyInfoModel.getName());
        companyAddress.setText(companyInfoModel.getAddr());
        companyEmailAddress.setText(companyInfoModel.getEmail());
        companyWebsite.setText(companyInfoModel.getUrl());
        lastTradedPrice.setText(companyInfoModel.getLtp());
        totalListedShared.setText(companyInfoModel.getTls());
        paidUpValue.setText(companyInfoModel.getPuv());
        reverse.setText(companyInfoModel.getReverse());
        eps.setText(companyInfoModel.getEps());
        pe.setText(companyInfoModel.getPv());
        bookValue.setText(companyInfoModel.getBookValue());

        if(companyInfoModel.getDiff().contains("-")){
            changes.setTextColor(ContextCompat.getColor(getActivity(),R.color.text_color_red));
        }
        changes.setText(companyInfoModel.getDiff() + "  (" + companyInfoModel.getDiffP()+")");

    }

    @Override
    public void noDataFound() {
        loading.setVisibility(View.GONE);
        mainLayout.setVisibility(View.GONE);
        noDatafound.setVisibility(View.VISIBLE);
    }
}
