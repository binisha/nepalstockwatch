package com.zeronebits.nepalstockwatch.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Own on 10/17/2017.
 */

public class Constant {

    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final java.lang.String COMPANYCODE = "company_code";
    public static final String ALERTSCATEGORY = "alerts_category";
    public static final String ALERTS = "alerts";
    public static String TOKEN="token";
    public static String USERCODE = "";
    public static String ACESS_TOKEN = "";
    public static ArrayList<String> indexList;
    public static ArrayList<String> subIndexList;
    public static ArrayList<String> companyList;




    public static void showJsonErrorMsgNewArray(Context context, String result){
        String msg = "";
        Log.d("djsfdsfdsf","herer");

        try{
            JSONObject errorMsg = new JSONObject(result);
//            JSONObject errorMsg = jsonObject.getJSONObject("message");
            for(int i=0; i < errorMsg.length(); i++){
                String message="";
                message = errorMsg.getString(errorMsg.names().getString(i));
                if(msg.equalsIgnoreCase("")){
                    msg = message;
                }else{
                    msg = message + "\n" + msg;
                }
            }
            String name = msg.replace("[","");
            String finalName = name.replace("]","");
            Toast.makeText(context, finalName, Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            e.printStackTrace();
            showJsonErrorMsgNew(context,result);
        }
    }


    public static void showJsonErrorMsgNew(Context context,String result){
        try{
            Log.d("djsfdsfdsf",result);
            JSONObject jsonObject = new JSONObject(result);
            String errorMsg = jsonObject.getString("message");
            Log.d("djsfdsfdsf",errorMsg);
            Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();

        }catch (Exception e){
//            CustomToast.showToastBottom(context,context.getString(R.string.couldnot_connect_to_server),CustomToast.TYPE_CANCEL);
        }
    }

    public static Bitmap changeImageToCircle(Bitmap bitmap) {

        Bitmap output;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

}
